// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Templates/SubclassOf.h"
#include "GameFramework/PlayerController.h"
#include "SON_PlayerController.generated.h"

/** Forward declaration to improve compiling times */
class UNiagaraSystem;
class UInputMappingContext;
class UInputAction;
struct FInputActionValue;

DECLARE_LOG_CATEGORY_EXTERN(LogTemplateCharacter, Log, All);

UENUM(BlueprintType)
enum class ESON_AbilityKeyType : uint8 {
	LMB UMETA(DisplayName = "LMB"),
	RMB	UMETA(DisplayName = "RMB"),
};

UCLASS()
class ASON_PlayerController : public APlayerController
{
	GENERATED_BODY()

	/*UPROPERTY(EditAnywhere)
	TSubclassOf<class ASON_Tower> Tower1Class;
	UPROPERTY(EditAnywhere)
	TSubclassOf<class ASON_Tower> Tower2Class;

	UPROPERTY()
	class ASON_BuildingTower* BuildingTower = nullptr;*/

	static const int ABILITY_QUEUE_LEN = 4;
	
	ESON_AbilityKeyType AbilityQueue[ABILITY_QUEUE_LEN];
	int CurAbilityQueueIndex = 0;
	FTimerHandle AbilityQueueRefreshTimer;
	float AbilityQueueRefreshTime = 1.f;

public:
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAbilityQueueUpdate);
	UPROPERTY(BlueprintAssignable)
	FOnAbilityQueueUpdate OnAbilityQueueUpdate;

	ASON_PlayerController();

	UFUNCTION(BlueprintCallable)
	TArray<ESON_AbilityKeyType> GetAbilityQueue() const;

	UFUNCTION(BlueprintCallable)
	int GetAbilityQueueLen() const {return ABILITY_QUEUE_LEN;}

	/** Time Threshold to know if it was a short press */
	/*UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float ShortPressThreshold;*/

	/** FX Class that we will spawn when clicking */
	/*UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UNiagaraSystem* FXCursor;*/

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* SetLMBClickAction;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* SetRMBClickAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* SetMoveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UNiagaraSystem* CursorDecalSystem;

	class UNiagaraComponent* CursorDecal;
	/*UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* SetBuildTower1Action;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* SetBuildTower2Action;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class ASON_BuildingTower> BuildingTowerClass;*/


	bool IsHitUnderCursor() const { return bHitCursor; }
	const FHitResult &HitResultUnderCursor() const { return HitUnderCursor; }


protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	//uint32 bMoveToMouseCursor : 1;

	virtual void SetupInputComponent() override;
	
	// To add mapping context
	virtual void BeginPlay();

	virtual void Tick(float DeltaSeconds);

	/** Input handlers for SetDestination action. */
	//void OnInputStarted();
	//void OnSetDestinationTriggered();
	//void OnSetDestinationReleased();
	void OnLMBClickStart();
	void OnRMBClickStart();
	void OnMove(const FInputActionValue& Value);
	/*void OnBuildTower1();
	void OnBuildTower2();

	void StartBuildTower(const TSubclassOf<ASON_Tower>& TowerClass);*/

	void CheckAbilityActivation();

	void OnRefreshAbilityQueue();

private:
	//FVector CachedDestination;
	FHitResult HitUnderCursor;
	bool bHitCursor = false;

	//float FollowTime; // For how long it has been pressed
};



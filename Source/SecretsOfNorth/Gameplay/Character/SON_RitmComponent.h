// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SON_RitmComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SECRETSOFNORTH_API USON_RitmComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USON_RitmComponent();

	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRitmCheckValidEvent);
	UPROPERTY(BlueprintAssignable)
	FOnRitmCheckValidEvent OnRitmCheckValidEvent;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY()
	class UDecalComponent* StaticDecal;
	UPROPERTY()
	class UDecalComponent* DynamicDecal;

	UPROPERTY(EditAnywhere)
	class UMaterialInterface* DecalMaterial;
	UPROPERTY(EditAnywhere)
	float Height = 100;
	UPROPERTY(EditAnywhere)
	float MinRadius = 100;
	UPROPERTY(EditAnywhere)
	float MaxRadius = 200;
	UPROPERTY(EditAnywhere)
	float PeriodSec = 0.5;
	UPROPERTY(EditAnywhere)
	float ValidDelta = 0.2;
	UPROPERTY(EditAnywhere)
	float InvalidationPeriod = 0.3;

	FLinearColor DefaultColor = FLinearColor::White;
	FLinearColor TrueColor = FLinearColor::Green;
	FLinearColor FalseColor = FLinearColor::Red;

	FTimerHandle ColorChangeTimer;

	float Time = 1;
	float LastCheckTime = -1;

	UFUNCTION()
	void SetDecalColor(struct FLinearColor Color);

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	bool Check();
};

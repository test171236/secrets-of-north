// Copyright Epic Games, Inc. All Rights Reserved.

#include "SON_PlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "SON_PlayerCharacter.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "InputActionValue.h"
#include "EnhancedInputSubsystems.h"
#include "Engine/LocalPlayer.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "../Towers/SON_BuildingTower.h"
#include "../Towers/SON_Tower.h"
#include "SON_RitmComponent.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

ASON_PlayerController::ASON_PlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	//CachedDestination = FVector::ZeroVector;
	//FollowTime = 0.f;
}

TArray<ESON_AbilityKeyType> ASON_PlayerController::GetAbilityQueue() const
{
	TArray<ESON_AbilityKeyType>	res;

	for (int i = 0; i < CurAbilityQueueIndex; i++)
		res.Add(AbilityQueue[i]);

	return res;
}

void ASON_PlayerController::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}

	CursorDecal = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), CursorDecalSystem, FVector());
}

void ASON_PlayerController::Tick(float DeltaSeconds)
{
	bHitCursor = GetHitResultUnderCursor(ECollisionChannel::ECC_GameTraceChannel1, true, HitUnderCursor);
	if (bHitCursor && IsValid(CursorDecal))
		CursorDecal->SetWorldLocation(HitUnderCursor.ImpactPoint);
	/*if (IsValid(BuildingTower) && IsValid(GetPawn()))
	{
		BuildingTower->SetActorLocation(HitUnderCursor.ImpactPoint +
			FVector(0, 0, BuildingTower->GetCapsule()->GetScaledCapsuleHalfHeight() + 0.1));
		BuildingTower->SetActorRotation(GetPawn()->GetActorRotation());
	} */

	ASON_PlayerCharacter* player = Cast<ASON_PlayerCharacter>(GetPawn());

	if (IsValid(player))
		SetAudioListenerOverride(nullptr, player->GetActorLocation(), player->GetTopDownCameraComponent()->GetComponentRotation());
}

void ASON_PlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent))
	{
		// Setup mouse input events
		EnhancedInputComponent->BindAction(SetLMBClickAction, ETriggerEvent::Started, this, &ASON_PlayerController::OnLMBClickStart);
		EnhancedInputComponent->BindAction(SetRMBClickAction, ETriggerEvent::Started, this, &ASON_PlayerController::OnRMBClickStart);
		//EnhancedInputComponent->BindAction(SetClickAction, ETriggerEvent::Triggered, this, &ASON_PlayerController::OnSetDestinationTriggered);
		//EnhancedInputComponent->BindAction(SetClickAction, ETriggerEvent::Completed, this, &ASON_PlayerController::OnSetDestinationReleased);
		//EnhancedInputComponent->BindAction(SetClickAction, ETriggerEvent::Canceled, this, &ASON_PlayerController::OnSetDestinationReleased);
	
		EnhancedInputComponent->BindAction(SetMoveAction, ETriggerEvent::Triggered, this, &ASON_PlayerController::OnMove);
		
		//EnhancedInputComponent->BindAction(SetBuildTower1Action, ETriggerEvent::Completed, this, &ASON_PlayerController::OnBuildTower1);
		//EnhancedInputComponent->BindAction(SetBuildTower1Action, ETriggerEvent::Canceled, this, &ASON_PlayerController::OnBuildTower1);

		//EnhancedInputComponent->BindAction(SetBuildTower2Action, ETriggerEvent::Completed, this, &ASON_PlayerController::OnBuildTower2);
		//EnhancedInputComponent->BindAction(SetBuildTower2Action, ETriggerEvent::Canceled, this, &ASON_PlayerController::OnBuildTower2);
	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input Component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}

/*void ASON_PlayerController::OnInputStarted()
{
	//StopMovement();
}

// Triggered every frame when the input is held down
void ASON_PlayerController::OnSetDestinationTriggered()
{
	// We flag that the input is being pressed
	FollowTime += GetWorld()->GetDeltaSeconds();
	
	// We look for the location in the world where the player has pressed the input
	FHitResult Hit;
	bool bHitSuccessful = false;
	bHitSuccessful = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, Hit);

	// If we hit a surface, cache the location
	if (bHitSuccessful)
	{
		CachedDestination = Hit.Location;
	}
	
	// Move towards mouse pointer or touch
	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn != nullptr)
	{
		FVector WorldDirection = (CachedDestination - ControlledPawn->GetActorLocation()).GetSafeNormal();
		//ControlledPawn->AddMovementInput(WorldDirection, 1.0, false);
	}
}

void ASON_PlayerController::OnSetDestinationReleased()
{
	if (IsHitUnderCursor() && IsValid(BuildingTower) && IsValid(GetPawn()))
	{
		FHitResult hr = HitResultUnderCursor();
		ASON_Tower* tower = BuildingTower->Build(FTransform(
			GetPawn()->GetActorRotation(),
			hr.ImpactPoint +
			FVector(0, 0, BuildingTower->GetCapsule()->GetScaledCapsuleHalfHeight()),
			FVector(1.f)));

		if (IsValid(tower))
			BuildingTower->Destroy();
	}

	// If it was a short press
	if (FollowTime <= ShortPressThreshold)
	{
		// We move there and spawn some particles
		//UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, CachedDestination);
		//UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, CachedDestination, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	}

	FollowTime = 0.f;
}*/

void ASON_PlayerController::OnLMBClickStart()
{
	ASON_PlayerCharacter* player = Cast<ASON_PlayerCharacter>(GetPawn());

	if (IsValid(player) && IsValid(player->RitmComponent))
	{
		if (player->RitmComponent->Check())
		{
			if (CurAbilityQueueIndex < ABILITY_QUEUE_LEN)
			{
				AbilityQueue[CurAbilityQueueIndex] = ESON_AbilityKeyType::LMB;
				CurAbilityQueueIndex++;
				if (CurAbilityQueueIndex == ABILITY_QUEUE_LEN)
				{
					CheckAbilityActivation();
					CurAbilityQueueIndex = 0;
				}
				else
				{
					GetWorld()->GetTimerManager().SetTimer(
						AbilityQueueRefreshTimer,
						this,
						&ASON_PlayerController::OnRefreshAbilityQueue,
						AbilityQueueRefreshTime);
				}
				OnAbilityQueueUpdate.Broadcast();
			}
		}
		else
			OnRefreshAbilityQueue();
	}
}

void ASON_PlayerController::OnRMBClickStart()
{
	ASON_PlayerCharacter* player = Cast<ASON_PlayerCharacter>(GetPawn());

	if (IsValid(player) && IsValid(player->RitmComponent))
	{
		if (player->RitmComponent->Check())
		{
			if (CurAbilityQueueIndex < ABILITY_QUEUE_LEN)
			{
				AbilityQueue[CurAbilityQueueIndex] = ESON_AbilityKeyType::RMB;
				CurAbilityQueueIndex++;
				if (CurAbilityQueueIndex == ABILITY_QUEUE_LEN)
				{
					CheckAbilityActivation();
					CurAbilityQueueIndex = 0;
				}
				else
				{
					GetWorld()->GetTimerManager().SetTimer(
						AbilityQueueRefreshTimer,
						this,
						&ASON_PlayerController::OnRefreshAbilityQueue,
						AbilityQueueRefreshTime);
				}
				OnAbilityQueueUpdate.Broadcast();
			}
		}
		else
			OnRefreshAbilityQueue();
	}
}

void ASON_PlayerController::OnMove(const FInputActionValue& Value)
{
	const FVector2D MoveValue = Value.Get<FVector2D>();

	ASON_PlayerCharacter* ControlledPawn = Cast<ASON_PlayerCharacter>(GetPawn());
	if (ControlledPawn != nullptr && MoveValue.SquaredLength() > 0)
	{
		APlayerCameraManager* PlayerCamera = GetWorld()->GetFirstPlayerController()->PlayerCameraManager;
		
		FVector ForvardV = PlayerCameraManager->GetActorForwardVector();
		FVector RightV = PlayerCameraManager->GetActorRightVector();

		ForvardV = FVector(ForvardV.X, ForvardV.Y, 0).GetSafeNormal();
		RightV = FVector(RightV.X, RightV.Y, 0).GetSafeNormal();

		FVector WorldDirection = (RightV * MoveValue.X + ForvardV * MoveValue.Y).GetSafeNormal();
		ControlledPawn->AddMovementInput(WorldDirection, 1.0, false);
	}
}

void ASON_PlayerController::CheckAbilityActivation()
{
	if (CurAbilityQueueIndex != ABILITY_QUEUE_LEN)
		return;

	ASON_PlayerCharacter* ControlledPawn = Cast<ASON_PlayerCharacter>(GetPawn());
	if (ControlledPawn != nullptr)
	{
		ControlledPawn->AbilitySystem->ActivateAbility(AbilityQueue, ABILITY_QUEUE_LEN);
	}
}

void ASON_PlayerController::OnRefreshAbilityQueue()
{
	CurAbilityQueueIndex = 0;
	OnAbilityQueueUpdate.Broadcast();
}

/*void ASON_PlayerController::StartBuildTower(const TSubclassOf<ASON_Tower>& TowerClass)
{
	if (IsValid(BuildingTower) && BuildingTower->TowerClass == TowerClass)
	{
		BuildingTower->Destroy();
		return;
	}

	if (IsValid(GetPawn()))
	{
		if (IsValid(BuildingTower))
			BuildingTower->Destroy();

		BuildingTower = GetWorld()->SpawnActor<ASON_BuildingTower>(
			BuildingTowerClass,
			FTransform(GetPawn()->GetActorLocation()));

		if (IsValid(BuildingTower))
			BuildingTower->SetupTowerClass(TowerClass);
	}
}

void ASON_PlayerController::OnBuildTower1()
{
	StartBuildTower(Tower1Class);
}

void ASON_PlayerController::OnBuildTower2()
{
	StartBuildTower(Tower2Class);
}*/

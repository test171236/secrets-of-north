// Fill out your copyright notice in the Description page of Project Settings.

#include "SON_AbilitySystem.h"

#include "Components/AudioComponent.h"

#include "../../Game/SON_GameInstance.h"
#include "../../Game/SON_SaveGame.h"

USON_AbilitySystem::USON_AbilitySystem()
{
	PrimaryComponentTick.bCanEverTick = false;
}

USON_AbilitySystem::~USON_AbilitySystem()
{
	DestroyAbilityRoots();
}

void USON_AbilitySystem::BeginPlay()
{
	Super::BeginPlay();

	TArray<FSON_AbilityData*> data;

	AbilityDataTable->GetAllRows<FSON_AbilityData>("", data);

	CreateAbilityRoots();

	for (auto& abil : data)
	{
		FSON_AbilityAudioData aad;
		aad.Name = abil->Name;
		aad.AudioComponent = NewObject<UAudioComponent>(this, FName(aad.Name.ToString() + "Audio"));
		aad.AudioComponent->Sound = abil->Audio;
		aad.AudioComponent->RegisterComponent();
		aad.AudioComponent->Activate();
		aad.AudioComponent->SetVolumeMultiplier(0.01f);
		AbilityAudioData.Add(aad);
	}

	AbilityArray.Empty();

	USON_GameInstance* gi = Cast<USON_GameInstance>(GetOwner()->GetGameInstance());
	FSON_AbilityData* BasicAbilityData = nullptr;
	SON_AbilityTreeNode* BasicAbilityRootNode = nullptr;

	for (auto& root : AbilityRoots)
		if (root->FindpNodeByName(BasicAbilityName))
		{
			BasicAbilityRootNode = root;
			break;
		}
	if (IsValid(gi) && gi->GameData)
	{
		for (auto& all_abil : data)
		{
			int ind = -1;
			for (auto& abs : gi->GameData->AbilityBonusSave)
				if (all_abil->Name == abs.Name)
				{
					if (BasicAbilityRootNode->FindpNodeByName(abs.Name))
					{
						BasicAbility = *all_abil;
						BasicAbility.Ability = NewObject<USON_PlayerAbility>(this, all_abil->AbilityClass, abs.Name);
						BasicAbility.Ability->SetupAbility(abs.Name, BasicAbility.BaseParams, abs.AbilityBonusParameters);
						BasicAbility.Ability->RegisterComponent();
						BasicAbility.Ability->ActivateAbility();
					} else {
						ind = AbilityArray.Add(*all_abil);
						AbilityArray[ind].Ability = NewObject<USON_PlayerAbility>(this, AbilityArray[ind].AbilityClass, abs.Name);
						AbilityArray[ind].Ability->SetupAbility(abs.Name, AbilityArray[ind].BaseParams, abs.AbilityBonusParameters);
						AbilityArray[ind].Ability->RegisterComponent();
					}
					break;
				}
			if (all_abil->Name == BasicAbilityName)
				BasicAbilityData = all_abil;
		}	
	}

	if (!IsValid(BasicAbility.Ability) && BasicAbilityData)
	{
		BasicAbility = *BasicAbilityData;
		BasicAbility.Ability = NewObject<USON_PlayerAbility>(this, BasicAbilityData->AbilityClass, BasicAbilityName);
		BasicAbility.Ability->SetupAbility(BasicAbilityName, BasicAbility.BaseParams, FSON_AbilityBonusParameters());
		BasicAbility.Ability->RegisterComponent();
		BasicAbility.Ability->ActivateAbility();
	}
}

void USON_AbilitySystem::CreateAbilityRoots()
{
	TArray<FSON_AbilityData*> data;

	AbilityDataTable->GetAllRows<FSON_AbilityData>("", data);

	for (auto& abil : data)
	{
		SON_AbilityTreeNode* node = new SON_AbilityTreeNode(abil);

		for (int i = 0; i < AbilityRoots.Num(); i++)
		{
			int32 ind;
			if (node->AbilityData->NextAbilities.Find(AbilityRoots[i]->AbilityData->Name, ind))
			{
				node->Next.Add(AbilityRoots[i]);
				AbilityRoots.RemoveAt(i);
				i--;
			}
		}

		bool isRoot = true;

		for (auto& root : AbilityRoots)
		{
			SON_AbilityTreeNode* res = root->FindpParentNode(node->AbilityData->Name);
			if (res != nullptr)
			{
				res->Next.Add(node);
				isRoot = false;
			}
		}

		if (isRoot)
			AbilityRoots.Add(node);
	}
}

void USON_AbilitySystem::DestroyAbilityRoots()
{
	for (auto& root : AbilityRoots)
		delete root;
	AbilityRoots.Empty();
}

void USON_AbilitySystem::UpdateAvaliableBonuses(const TArray<struct FSON_BonusData>& AllBonuses, TArray<struct FSON_BonusData>& out) const
{
	for (auto& bonus : AllBonuses)
		if (bonus.Type == ESON_BonusType::AbilityBonus)
		{
			{
				FRegexPattern pattern(bonus.AbilityName.ToString());
				FRegexMatcher matcher(pattern, BasicAbility.Name.ToString());
				if (matcher.FindNext())
				{
					out.Add(bonus);
					continue;
				}	 
			}
			for (auto& abil : AbilityArray)
			{
				FRegexPattern pattern(bonus.AbilityName.ToString());
				FRegexMatcher matcher(pattern, abil.Name.ToString());
				if (matcher.FindNext())
				{
					out.Add(bonus);
					break;
				}
			}
		}
}

void USON_AbilitySystem::UpdateAbilityUnlockBonuses(TArray<struct FSON_BonusData>& out, bool AddNewAbilities) const
{
	for (auto& root : AbilityRoots)
	{
		SON_AbilityTreeNode* res = root->FindpNodeByName(BasicAbility.Name);
		if (res == nullptr)
		{
			for (auto& abil : AbilityArray)
				if ((res = root->FindpNodeByName(abil.Name)) != nullptr)
					break;
			if (res == nullptr && AddNewAbilities)
			{
				FSON_BonusData bd;
				bd.Type = ESON_BonusType::UnlockAbility;
				bd.AbilityName = root->AbilityData->Name;
				bd.Description = root->AbilityData->Description;
				out.Add(bd);
			}
		}
		
		if (res != nullptr)
			for (auto& next_node : res->Next)
			{
				FSON_BonusData bd;
				bd.Type = ESON_BonusType::UnlockAbility;
				bd.AbilityName = next_node->AbilityData->Name;
				bd.Description = next_node->AbilityData->Description;
				out.Add(bd);
			}
	}
}

void USON_AbilitySystem::ApplyBonus(FName Name, const FSON_AbilityBonusParameters& Bonus)
{
	{
		FRegexPattern pattern(Name.ToString());
		FRegexMatcher matcher(pattern, BasicAbility.Name.ToString());
		if (matcher.FindNext())
			BasicAbility.Ability->ApplyBonus(Bonus);
	}

	for (auto& abil : AbilityArray)
	{
		FRegexPattern pattern(Name.ToString());
		FRegexMatcher matcher(pattern, abil.Name.ToString());
		if (matcher.FindNext())
			abil.Ability->ApplyBonus(Bonus);
	}
}

USON_PlayerAbility* USON_AbilitySystem::GetAbilityByName(FName Name) const
{
	if (Name == BasicAbility.Name)
		return BasicAbility.Ability;

	int ind = GetAbilityDataIndexByName(Name);
	if (ind < 0)
		return nullptr;

	return AbilityArray[ind].Ability;
}

void USON_AbilitySystem::DeactivateAbilities()
{
	for (auto& aa : AbilityArray)
		aa.Ability->DeactivateAbility();
	BasicAbility.Ability->DeactivateAbility();
}

void USON_AbilitySystem::Empty()
{
	AbilityArray.Empty();

	FSON_AbilityData* BasicAbilityData = nullptr;
	TArray<FSON_AbilityData*> data;

	AbilityDataTable->GetAllRows<FSON_AbilityData>("", data);
	for (auto& all_abil : data)
		if (all_abil->Name == BasicAbilityName)
			BasicAbilityData = all_abil;

	if (BasicAbilityData)
	{
		BasicAbility = *BasicAbilityData;
		BasicAbility.Ability = NewObject<USON_PlayerAbility>(this, BasicAbilityData->AbilityClass, BasicAbilityName);
		BasicAbility.Ability->SetupAbility(BasicAbilityName, BasicAbility.BaseParams, FSON_AbilityBonusParameters());
		BasicAbility.Ability->RegisterComponent();
		//BasicAbility.Ability->ActivateAbility();
	}
}

void USON_AbilitySystem::UnlockAbility(FName newAbilityName)
{
	SON_AbilityTreeNode* parent = nullptr;

	for (auto& root : AbilityRoots)
		if ((parent = root->FindpParentNode(newAbilityName)) != nullptr)
			break;

	SON_AbilityTreeNode* node = nullptr;

	for (auto& root : AbilityRoots)
		if ((node = root->FindpNodeByName(newAbilityName)) != nullptr)
			break;

	if (node)
	{
		if (parent == nullptr)
		{
			int ind = AbilityArray.Add(*node->AbilityData);
			FSON_AbilityBonusParameters abp;

			AbilityArray[ind].Ability = NewObject<USON_PlayerAbility>(this, AbilityArray[ind].AbilityClass, AbilityArray[ind].Name);
			AbilityArray[ind].Ability->SetupAbility(AbilityArray[ind].Name,
				AbilityArray[ind].BaseParams,
				abp);
			AbilityArray[ind].Ability->RegisterComponent();
		}
		else
		{
			if (parent->AbilityData->Name == BasicAbility.Name)
			{
				FSON_AbilityBonusParameters abp = BasicAbility.Ability->BonusParametrs;
				BasicAbility.Ability->DestroyComponent();

				BasicAbility = *node->AbilityData;
				BasicAbility.Ability = NewObject<USON_PlayerAbility>(this, BasicAbility.AbilityClass, BasicAbility.Name);
				BasicAbility.Ability->SetupAbility(BasicAbility.Name,
					BasicAbility.BaseParams,
					abp);
				BasicAbility.Ability->RegisterComponent();
				BasicAbility.Ability->ActivateAbility();
			}
			else
				for (auto& abil : AbilityArray)
					if (abil.Name == parent->AbilityData->Name)
					{
						FSON_AbilityBonusParameters abp = abil.Ability->BonusParametrs;
						abil.Ability->DestroyComponent();

						abil = *node->AbilityData;
						abil.Ability = NewObject<USON_PlayerAbility>(this, abil.AbilityClass, abil.Name);
						abil.Ability->SetupAbility(abil.Name,
							abil.BaseParams,
							abp);
						abil.Ability->RegisterComponent();

						break;
					}
		}
	}
}

void USON_AbilitySystem::PlayAbilitySound(FName Name)
{
	for (auto& aad : AbilityAudioData)
		if (aad.Name == Name)
		{
			aad.AudioComponent->SetVolumeMultiplier(1.f);
			//aad.AudioComponent->FadeIn();
		}
}

void USON_AbilitySystem::StopAbilitySound(FName Name)
{
	for (auto& aad : AbilityAudioData)
		if (aad.Name == Name)
		{
			aad.AudioComponent->SetVolumeMultiplier(0.01f);
		}
}

int USON_AbilitySystem::GetAbilityDataIndexByName(FName Name) const
{
	for (int i = 0; i < AbilityArray.Num(); i++)
		if (AbilityArray[i].Name == Name)
			return i;
	return -1;
}

void USON_AbilitySystem::ActivateAbility(ESON_AbilityKeyType* AbilityQueue, int AbilityQueueLen)
{
	for (int i = 0; i < AbilityArray.Num(); i++)
		if (AbilityArray[i].Queue.Num() == AbilityQueueLen)
		{
			int ind;
			for (ind = 0; ind < AbilityQueueLen; ind++)
				if (AbilityQueue[ind] != AbilityArray[i].Queue[ind])
					break;
			USON_PlayerAbility* abil = AbilityArray[i].Ability;
			if (ind == AbilityQueueLen && IsValid(abil))
			{
				abil->ActivateAbility();
				OnAbilityActivation.Broadcast(AbilityArray[i]);
			}
		}
}

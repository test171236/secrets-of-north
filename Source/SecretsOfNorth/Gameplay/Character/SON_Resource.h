// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SON_Resource.generated.h"

UENUM(BlueprintType)
enum class ESON_ResourceType : uint8 {
	Default       UMETA(DisplayName = "Default"),
};

UCLASS()
class SECRETSOFNORTH_API ASON_Resource : public AActor
{
	GENERATED_BODY()
	
	enum class ESON_ResourceState {
		Falling,
		Pickable,
		Picked
	} State = ESON_ResourceState::Falling;

	float FlyingSpeed = 1000;

public:	
	// Sets default values for this actor's properties
	ASON_Resource();

	void Pick();
	virtual void Tick(float deltaTime) override;
	int GetAmount() { return Amount; }

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ESON_ResourceType ResourceType = ESON_ResourceType::Default;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Amount = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USphereComponent* CollisionSphere;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UStaticMeshComponent* StaticMesh;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UFUNCTION()
	void OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);

};

// Copyright Epic Games, Inc. All Rights Reserved.

#include "SON_PlayerCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/AudioComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMaterialLibrary.h"
#include "SON_PlayerController.h"
#include "../Common/SON_HealthSystem.h"
#include "../Common/SON_Projectile.h"
#include "../Enemies/SON_Enemy.h"
#include "../../Game/SON_AutobattleGameMode.h"
#include "../../Game/SON_GameInstance.h"
#include "../../Game/SON_SaveGame.h"
#include "../Common/Abilities/SON_ShootProjectileAbility.h"
#include "../Common/Abilities/SON_SpawnActorOnCursorAbility.h"
#include "SON_Resource.h"
#include "SON_RitmComponent.h"

ASON_PlayerCharacter::ASON_PlayerCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	GetCapsuleComponent()->SetCollisionProfileName(FName("PlayerCharacter"));

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	HealthSystem = CreateDefaultSubobject<USON_HealthSystem>(TEXT("HealthSystem"));
	HealthSystem->SetMaxHP(5.f);

	//ShootProjectileAbility = CreateDefaultSubobject<USON_ShootProjectileAbility>(TEXT("ShootProjectileAbility"));
	//ShootWaveAbility = CreateDefaultSubobject<USON_ShootProjectileAbility>(TEXT("ShootWaveAbility"));
	//SpawnExplosionAbility = CreateDefaultSubobject<USON_SpawnActorOnCursorAbility>(TEXT("SpawnExplosionAbility"));

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	OnActorBeginOverlap.AddDynamic(this, &ASON_PlayerCharacter::BeginOverlap);
	HealthSystem->OnDied.AddDynamic(this, &ASON_PlayerCharacter::Death);

	RitmComponent = CreateDefaultSubobject<USON_RitmComponent>(TEXT("RitmComponent"));
	AbilitySystem = CreateDefaultSubobject<USON_AbilitySystem>(TEXT("AbilitySystem"));
}

void ASON_PlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	ASON_AutobattleGameMode* gm = Cast<ASON_AutobattleGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(gm))
	{
		gm->SetupPlayerCharacter(this);
	}

	InitCharacter();

	InitAllBonusesFromTable();
}

void ASON_PlayerCharacter::EndPlay(EEndPlayReason::Type Reason)
{
	ASON_AutobattleGameMode* gm = Cast<ASON_AutobattleGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(gm) && (gm->GetState() == ESON_AutobattleGameModeState::Win || gm->GetState() == ESON_AutobattleGameModeState::Lose))
	{
		USON_GameInstance* gi = Cast<USON_GameInstance>(GetGameInstance());
		if (IsValid(gi) && gi->GameData)
		{
			gi->GameData->CharacterBonuses = Bonuses;
			gi->GameData->CharacterStats = Stats;
			gi->GameData->CharacterLevel = Level;

			gi->GameData->AbilityBonusSave.Empty();
			gi->SetAbilityBonusByName(AbilitySystem->GetBasicAbility().Name, AbilitySystem->GetBasicAbility().Ability->BonusParametrs);
			for (auto& ac : AbilitySystem->GetAbilityArray())
				gi->SetAbilityBonusByName(ac.Name, ac.Ability->BonusParametrs);
		}
	}
	Super::EndPlay(Reason);
}

void ASON_PlayerCharacter::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
}

void ASON_PlayerCharacter::EmptyBonuses()
{
	Bonuses = FSON_CharacterStats();
	Level = 0;
	AbilitySystem->Empty();

	HealthSystem->ChangeMaxHP(Base.HP + Stats.HP + Bonuses.HP);
	GetCharacterMovement()->MaxWalkSpeed = Base.Speed + Stats.Speed + Bonuses.Speed;
}

void ASON_PlayerCharacter::ApplyBonus(const FSON_BonusData& BonusData)
{
	switch (BonusData.Type)
	{
	case ESON_BonusType::CharacterBonus:
		{
			Bonuses += BonusData.CharacterBonuses;
			HealthSystem->ChangeMaxHP(GetCharacterMaxHP());
			HealthSystem->AddHP(BonusData.CharacterBonuses.HP);
			GetCharacterMovement()->MaxWalkSpeed = GetCharacterSpeed();
		}
		break;
	case ESON_BonusType::UnlockAbility:
		AbilitySystem->UnlockAbility(BonusData.AbilityName);
		break;
	case ESON_BonusType::AbilityBonus:
		AbilitySystem->ApplyBonus(BonusData.AbilityName, BonusData.AbilityBonuses);
		break;
	default:
		break;
	}
}

void ASON_PlayerCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	ASON_PlayerController* control = Cast<ASON_PlayerController>(GetController());
	if (IsValid(control) && control->IsHitUnderCursor())
	{
		FHitResult hr = control->HitResultUnderCursor();
		FVector dir = hr.ImpactPoint - GetActorLocation();
		FRotator rot = FVector(dir.X, dir.Y, 0).Rotation();

		SetActorRotation(rot);
	} 

	TArray<AActor*> out;

	if (UKismetSystemLibrary::SphereOverlapActors(GetWorld(),
		GetActorLocation(),
		GetCharacterPickUpRadius(),
		TArray<TEnumAsByte<EObjectTypeQuery>>({ UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel6) }),
		ASON_Resource::StaticClass(),
		TArray<AActor*>(),
		out))
		for (auto i : out)
		{
			ASON_Resource* res = Cast<ASON_Resource>(i);
			if (IsValid(res))
				res->Pick();
		}

	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), CellMPC, TEXT("PlayerPos"), FLinearColor(GetActorLocation()));
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), CellMPC, TEXT("CameraPos"), FLinearColor(TopDownCameraComponent->GetComponentLocation()));

	//ASON_PlayerController* control = Cast<ASON_PlayerController>(GetController());
	if (IsValid(control) && control->IsHitUnderCursor())
		UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), CellMPC, TEXT("CursorPos"), 
			FLinearColor(control->HitResultUnderCursor().ImpactPoint));

	HealthSystem->AddHP(GetCharacterRegen() * DeltaSeconds);
}

void ASON_PlayerCharacter::BeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	ASON_Resource* res = Cast<ASON_Resource>(OtherActor);
	if (IsValid(res))
	{
		CurEXP += res->GetAmount() * GetCharacterXPMul();
		res->Destroy();
		if (CurEXP >= MaxEXP)
			LevelUP();
		OnPickUpEXP.Broadcast();
	}
}

TArray<FSON_BonusData> ASON_PlayerCharacter::GetAvaliableBonuses()
{
	UpdateAvaliableBonuses();
	UpdateAbilityUnlockBonuses();

	const int BonusNum = 3;
	TArray<FSON_BonusData> bonuses;
	for (int i = 0; i < BonusNum; i++)
	{
		if (AbilityUnlockBonuses.Num() > 0)
		{
			int ind = FMath::Rand() % AbilityUnlockBonuses.Num();
			bonuses.Add(AbilityUnlockBonuses[ind]);
			AbilityUnlockBonuses.RemoveAt(ind);
		} else {
			int ind = FMath::Rand() % AvaliableBonuses.Num();
			bonuses.Add(AvaliableBonuses[ind]);
			AvaliableBonuses.RemoveAt(ind);
		}
	}
	return bonuses;
}

void ASON_PlayerCharacter::LevelUP()
{
	if (CurEXP < MaxEXP)
		return;

	CurEXP = 0;
	MaxEXP += AddEXPonLVLUp;
	Level++;
	UpdateAvaliableBonuses();
	
	const int BonusNum = 3;
	TArray<FSON_BonusData> bonuses;
	ASON_AutobattleGameMode* gm = Cast<ASON_AutobattleGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	//if (Level % LevelsToAbilityUnlock != 0)
	if (IsValid(gm) && AbilitySystem->GetAbilityArray().Num() >= gm->GetMaxPlayerAbilityNum())
		for (int i = 0; i < BonusNum; i++)
		{
			int ind = FMath::Rand() % AvaliableBonuses.Num();
			bonuses.Add(AvaliableBonuses[ind]);
			AvaliableBonuses.RemoveAt(ind);
		}
	else
	{
		UpdateAbilityUnlockBonuses();
		for (int i = 0; i < BonusNum; i++)
		{
			if (AbilityUnlockBonuses.Num() > 0)
			{
				int ind = FMath::Rand() % AbilityUnlockBonuses.Num();
				bonuses.Add(AbilityUnlockBonuses[ind]);
				AbilityUnlockBonuses.RemoveAt(ind);
			} else {
				int ind = FMath::Rand() % AvaliableBonuses.Num();
				bonuses.Add(AvaliableBonuses[ind]);
				AvaliableBonuses.RemoveAt(ind);
			}
		}
	}
	OnLevelUP.Broadcast(bonuses);
}

void ASON_PlayerCharacter::InitAllBonusesFromTable()
{
	TArray<FSON_BonusData*>	data;

	BonusDataTable->GetAllRows<FSON_BonusData>("", data);

	AllBonuses.Empty();
	for (auto& bonus : data)
		AllBonuses.Add(*bonus);
}

void ASON_PlayerCharacter::InitCharacter()
{
	USON_GameInstance* gi = Cast<USON_GameInstance>(GetGameInstance());
	if (IsValid(gi) && gi->GameData)
	{
		Stats = gi->GameData->CharacterStats;
		Bonuses = gi->GameData->CharacterBonuses;
		Level = gi->GameData->CharacterLevel;
		MaxEXP += AddEXPonLVLUp * Level;
	}

	HealthSystem->SetMaxHP(GetCharacterMaxHP());
	GetCharacterMovement()->MaxWalkSpeed = GetCharacterSpeed();
}

void ASON_PlayerCharacter::UpdateAvaliableBonuses()
{		   
	AvaliableBonuses.Empty();
	AbilitySystem->UpdateAvaliableBonuses(AllBonuses, AvaliableBonuses);
	for (auto& bonus : AllBonuses)
		if (bonus.Type == ESON_BonusType::CharacterBonus)
			AvaliableBonuses.Add(bonus);
}

void ASON_PlayerCharacter::UpdateAbilityUnlockBonuses()
{
	AbilityUnlockBonuses.Empty();
	AbilitySystem->UpdateAbilityUnlockBonuses(AbilityUnlockBonuses, 
		AbilitySystem->GetAbilityArray().Num() < MaxAbilityNum);
}

float ASON_PlayerCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float res = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (HealthSystem->ReduceHP(res) > 0.f)
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), TakeDamageSound, GetActorLocation(), 1, 1, 0.2);

	return res;
}

void ASON_PlayerCharacter::Death()
{
	AbilitySystem->DeactivateAbilities();

	GetController()->StopMovement();
	GetCharacterMovement()->DisableMovement();

	PlayAnimMontage(DeathMontage);
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), DeathSound, GetActorLocation());
}

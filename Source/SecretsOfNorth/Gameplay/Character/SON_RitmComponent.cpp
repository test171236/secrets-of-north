// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_RitmComponent.h"

#include "Components/DecalComponent.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values for this component's properties
USON_RitmComponent::USON_RitmComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	StaticDecal = CreateDefaultSubobject<UDecalComponent>("StaticDecal");
	DynamicDecal = CreateDefaultSubobject<UDecalComponent>("DynamicDecal");
}


// Called when the game starts
void USON_RitmComponent::BeginPlay()
{
	Super::BeginPlay();

	StaticDecal->SetMaterial(0, DecalMaterial);
	DynamicDecal->SetMaterial(0, DecalMaterial);

	DynamicDecal->DecalSize = StaticDecal->DecalSize = FVector(Height, MinRadius, MinRadius);
	StaticDecal->SetWorldRotation(FVector(0, 0, -1).ToOrientationQuat());
	DynamicDecal->SetWorldRotation(FVector(0, 0, -1).ToOrientationQuat());
}


// Called every frame
void USON_RitmComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (IsValid(GetOwner()))
	{
		FVector loc = GetOwner()->GetActorLocation();
		FHitResult hr;

		UKismetSystemLibrary::LineTraceSingle(
			GetWorld(),
			loc,
			loc + FVector(0, 0, -1e6),
			UEngineTypes::ConvertToTraceType(ECC_GameTraceChannel1),
			false,
			TArray<AActor*>(),
			EDrawDebugTrace::None,
			hr,
			false);
		if (hr.bBlockingHit)
		{
			StaticDecal->SetWorldLocation(hr.ImpactPoint);
			DynamicDecal->SetWorldLocation(hr.ImpactPoint);
		}
	}

	Time -= DeltaTime / PeriodSec;
	if (Time < 0.f)
		Time += 1.f;

	float r = MinRadius + (MaxRadius - MinRadius) * Time;
	DynamicDecal->DecalSize = FVector(Height, r, r);
	DynamicDecal->UpdateComponentToWorld();
}

void USON_RitmComponent::SetDecalColor(FLinearColor Color)
{
	DynamicDecal->SetDecalColor(Color);
	StaticDecal->SetDecalColor(Color);
}

bool USON_RitmComponent::Check()
{
	if (GetWorld()->GetTimeSeconds() - LastCheckTime <= InvalidationPeriod)
	{
		LastCheckTime = GetWorld()->GetTimeSeconds();

		SetDecalColor(FalseColor);
		GetWorld()->GetTimerManager().SetTimer(
			ColorChangeTimer,
			[&]() { this->SetDecalColor(DefaultColor); },
			InvalidationPeriod,
			false);

		return false;
	}

	if (Time <= ValidDelta)
	{
		LastCheckTime = GetWorld()->GetTimeSeconds();
		SetDecalColor(TrueColor);
		GetWorld()->GetTimerManager().SetTimer(
			ColorChangeTimer,
			[&]() { this->SetDecalColor(DefaultColor); },
			InvalidationPeriod,
			false);

		OnRitmCheckValidEvent.Broadcast();
		return true;
	}

	SetDecalColor(FalseColor);
	GetWorld()->GetTimerManager().SetTimer(
		ColorChangeTimer,
		[&]() { this->SetDecalColor(DefaultColor); },
		InvalidationPeriod,
		false);

	LastCheckTime = GetWorld()->GetTimeSeconds();
	return false;
}


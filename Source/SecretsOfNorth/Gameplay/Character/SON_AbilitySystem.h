// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/DataTable.h"

#include "../Common/Abilities/SON_PlayerAbility.h"
#include "SON_AbilitySystem.generated.h"

class USON_PlayerAbility;
enum class ESON_AbilityKeyType : uint8;

USTRUCT(BlueprintType, Blueprintable)
struct FSON_AbilityData : public FTableRowBase {
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadOnly)
	USON_PlayerAbility* Ability;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName Name;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText InGameName;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<USON_PlayerAbility> AbilityClass;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FSON_AbilityBaseParameters BaseParams;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<ESON_AbilityKeyType> Queue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Description;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UPaperSprite* ReloadIcon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UPaperSprite* ActiveIcon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UPaperSprite* ReadyIcon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UPaperSprite* CardIcon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USoundBase* Audio;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FName> NextAbilities;
};

USTRUCT(BlueprintType)
struct FSON_AbilityAudioData {
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadOnly)
	FName Name;
	UPROPERTY(BlueprintReadOnly)
	class UAudioComponent* AudioComponent;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SECRETSOFNORTH_API USON_AbilitySystem : public UActorComponent
{
	GENERATED_BODY()

public:	
	USON_AbilitySystem();
	~USON_AbilitySystem();

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAbilityActivation, const FSON_AbilityData&, AbilityData);
	UPROPERTY(BlueprintAssignable)
	FOnAbilityActivation OnAbilityActivation;

	const FName BasicAbilityName = TEXT("BasicAbility");

	void ActivateAbility(ESON_AbilityKeyType* AbilityQueue, int AbilityQueueLen);

	void PlayAbilitySound(FName Name);
	void StopAbilitySound(FName Name);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const TArray<FSON_AbilityData>& GetAbilityArray() const { return AbilityArray; }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const FSON_AbilityData& GetBasicAbility() const { return BasicAbility; }

	void Empty();
	void UnlockAbility(FName newAbilityName);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetAbilityDataIndexByName(FName Name) const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	USON_PlayerAbility* GetAbilityByName(FName Name) const;

	void DeactivateAbilities();
	void UpdateAvaliableBonuses(const TArray<struct FSON_BonusData>& AllBonuses, TArray<struct FSON_BonusData>& out) const;
	void UpdateAbilityUnlockBonuses(TArray<struct FSON_BonusData>& out, bool AddNewAbilities) const;

	void ApplyBonus(FName Name, const struct FSON_AbilityBonusParameters& Bonus);

protected:
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly)
	TArray<FSON_AbilityData> AbilityArray;
	UPROPERTY(BlueprintReadOnly)
	FSON_AbilityData BasicAbility;
	UPROPERTY(BlueprintReadOnly)
	TArray<FSON_AbilityAudioData> AbilityAudioData;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UDataTable* AbilityDataTable;

	class SON_AbilityTreeNode
	{
	public:
		FSON_AbilityData* AbilityData;
		TArray<SON_AbilityTreeNode*> Next;

		SON_AbilityTreeNode(FSON_AbilityData* newAbilityData) : AbilityData(newAbilityData), Next() {}
		~SON_AbilityTreeNode()
		{
			for (auto& node : Next)
				delete node;
			Next.Empty();
		}

		SON_AbilityTreeNode* FindpParentNode(FName Name)
		{
			int32 ind;
			if (AbilityData->NextAbilities.Find(Name, ind))
				return this;
			
			SON_AbilityTreeNode* res = nullptr;
			for (auto& node : Next)
			{
				res = node->FindpParentNode(Name);
				if (res != nullptr)
					break;
			}
			return res;
		}

		SON_AbilityTreeNode* FindpNodeByName(FName Name)
		{
			if (AbilityData->Name == Name)
				return this;

			SON_AbilityTreeNode* res = nullptr;
			for (auto& node : Next)
			{
				res = node->FindpNodeByName(Name);
				if (res != nullptr)
					break;
			}
			return res;
		}

	};

	TArray<SON_AbilityTreeNode *> AbilityRoots;
	void CreateAbilityRoots();
	void DestroyAbilityRoots();

};

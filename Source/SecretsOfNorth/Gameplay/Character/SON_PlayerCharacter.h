// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Engine/DataTable.h"

#include "SON_AbilitySystem.h"
#include "../Common/SON_HealthSystem.h"
#include "SON_PlayerCharacter.generated.h"

USTRUCT(BlueprintType, Blueprintable)
struct FSON_CharacterStats {
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float HP = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Speed = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Damage = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float PickUpRadius = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Regen = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float XPMul = 0;


	FSON_CharacterStats& operator+= (const FSON_CharacterStats& cs)
	{
		HP += cs.HP;
		Speed += cs.Speed;
		Damage += cs.Damage;
		PickUpRadius += cs.PickUpRadius;
		Regen += cs.Regen;
		XPMul += cs.XPMul;

		return *this;
	}
};

UENUM(BlueprintType)
enum class ESON_BonusType : uint8 {
	AbilityBonus		UMETA(DisplayName = "AbilityBonus"),
	CharacterBonus      UMETA(DisplayName = "CharacterBonus"),
	UnlockAbility       UMETA(DisplayName = "UnlockAbility"),
};

USTRUCT(BlueprintType, Blueprintable)
struct FSON_BonusData : public FTableRowBase {
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	ESON_BonusType Type;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FSON_CharacterStats CharacterBonuses;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName AbilityName;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FSON_AbilityBonusParameters AbilityBonuses;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText Description;
};

UCLASS(Blueprintable)
class ASON_PlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLevelUP, const TArray<FSON_BonusData>&, AvailableBonuses);
	UPROPERTY(BlueprintAssignable)
	FOnLevelUP OnLevelUP;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPickUpEXP);
	UPROPERTY(BlueprintAssignable)
	FOnPickUpEXP OnPickUpEXP;

	const int MaxAbilityNum = 4;

	ASON_PlayerCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class USON_RitmComponent* RitmComponent;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class USON_AbilitySystem* AbilitySystem;

	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type Reason) override;
	virtual void OnConstruction(const FTransform& Transform) override;

	USON_HealthSystem::FOnDied& GetOnDiedDelegate() { return HealthSystem->OnDied; }

	UFUNCTION(BlueprintCallable)
	float GetCurEXP() const { return CurEXP; }
	UFUNCTION(BlueprintCallable)
	float GetMaxEXP() const { return MaxEXP; }
	UFUNCTION(BlueprintCallable)
	int GetPlayerLevel() const { return Level; }
	UFUNCTION(BlueprintCallable)
	void EmptyBonuses();
	UFUNCTION(BlueprintCallable)
	void ApplyBonus(const FSON_BonusData &BonusData);
	UFUNCTION(BlueprintCallable)
	TArray<FSON_BonusData> GetAvaliableBonuses();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FSON_CharacterStats Base;
	UPROPERTY(BlueprintReadWrite)
	FSON_CharacterStats Stats;
	UPROPERTY(BlueprintReadOnly)
	FSON_CharacterStats Bonuses;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCharacterMaxHP() const { return Base.HP + Stats.HP + Bonuses.HP; }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCharacterSpeed() const { return Base.Speed * (1 + Stats.Speed + Bonuses.Speed); }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCharacterDamage() const { return Stats.Damage + Bonuses.Damage; }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCharacterPickUpRadius() const { return Base.PickUpRadius * (1 + Stats.PickUpRadius + Bonuses.PickUpRadius); }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCharacterRegen() const { return Stats.Regen + Bonuses.Regen; }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCharacterXPMul() const { return 1 + Stats.XPMul + Bonuses.XPMul; }

	UPROPERTY(BlueprintReadOnly)
	TArray<FSON_BonusData> AllBonuses;
	UPROPERTY(BlueprintReadOnly)
	TArray<FSON_BonusData> AvaliableBonuses;
	UPROPERTY(BlueprintReadOnly)
	TArray<FSON_BonusData> AbilityUnlockBonuses;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UDataTable* BonusDataTable;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class USoundBase* TakeDamageSound;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class USoundBase* DeathSound;

	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USON_HealthSystem* HealthSystem;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UMaterialParameterCollection* CellMPC;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage* DeathMontage;

	/*UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USON_ShootProjectileAbility* ShootProjectileAbility;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USON_ShootProjectileAbility* ShootWaveAbility;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USON_SpawnActorOnCursorAbility* SpawnExplosionAbility;*/

	UPROPERTY(BlueprintReadOnly)
	float CurEXP = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxEXP = 10;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float AddEXPonLVLUp = 2;
	UPROPERTY(BlueprintReadOnly)
	int Level = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int LevelsToAbilityUnlock = 5;

	void LevelUP();
	void InitAllBonuses();
	void UpdateAvaliableBonuses();
	void UpdateAbilityUnlockBonuses();
	void InitAllBonusesFromTable();
	void InitCharacter();

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
	UFUNCTION()
	void BeginOverlap(AActor* OverlappedActor, AActor* OtherActor);
	UFUNCTION()
	void Death();
};


// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_Resource.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"

ASON_Resource::ASON_Resource()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->SetSphereRadius(10.f);
	CollisionSphere->SetCollisionProfileName(FName("Resource"));
	CollisionSphere->SetSimulatePhysics(true);
	CollisionSphere->SetCanEverAffectNavigation(false);
	SetRootComponent(CollisionSphere);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetCollisionProfileName(FName("NoCollision"));
	StaticMesh->SetupAttachment(CollisionSphere);
	StaticMesh->SetCanEverAffectNavigation(false);

	OnActorHit.AddDynamic(this, &ASON_Resource::OnHit);
}

void ASON_Resource::BeginPlay()
{
	Super::BeginPlay();
	
	FVector init_velocity = FVector(0, 0, 700).
		RotateAngleAxis(FMath::RandRange(0, 45), FVector(1, 0, 0)).
		RotateAngleAxis(FMath::RandRange(0, 360), FVector(0, 0, 1));

	CollisionSphere->SetPhysicsLinearVelocity(init_velocity);
}

void ASON_Resource::OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	State = ESON_ResourceState::Pickable;
	if(Hit.bBlockingHit && Hit.ImpactNormal.Equals(FVector(0,0,1)))
	{ 
		CollisionSphere->SetSimulatePhysics(false);
	}
}

void ASON_Resource::Pick()
{
	if (State != ESON_ResourceState::Pickable)
		return;

	State = ESON_ResourceState::Picked;
	CollisionSphere->SetSimulatePhysics(false);
	CollisionSphere->SetCollisionProfileName(FName("ResourceOverlap"));

	SetActorTickEnabled(true);
}

void ASON_Resource::Tick(float deltaTime)
{
	Super::Tick(deltaTime);

	ACharacter* ch = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	if (IsValid(ch))
	{
		FVector dir = (ch->GetActorLocation() - GetActorLocation()).GetSafeNormal();
		AddActorWorldOffset(dir * FlyingSpeed * deltaTime);
	}
}


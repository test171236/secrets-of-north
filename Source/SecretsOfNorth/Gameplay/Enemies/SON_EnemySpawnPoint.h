// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../../Game/SON_AutobattleGameMode.h"
#include "SON_EnemySpawnPoint.generated.h"

UCLASS()
class SECRETSOFNORTH_API ASON_EnemySpawnPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	ASON_EnemySpawnPoint();

	bool SpawnEnemy(const FSON_EnemyParameters &EnemyParams);
protected:
	virtual void BeginPlay() override;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "SON_RekkenController.generated.h"

UCLASS()
class SECRETSOFNORTH_API ASON_RekkenController : public AAIController
{
	GENERATED_BODY()
	
	UPROPERTY()
	AActor* Target;
	UPROPERTY()
	class ASON_Rekken* Rekken;

	FTimerHandle IdleTimer;

protected:
	virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;
	virtual void OnPossess(APawn* InPawn) override;
	virtual void Tick(float Delta) override;
public:
	ASON_RekkenController();
	void FindTarget();

};

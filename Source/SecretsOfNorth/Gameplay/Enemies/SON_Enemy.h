// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../Common/SON_DamageTypes.h"
#include "../../Game/SON_AutobattleGameMode.h"
#include "SON_Enemy.generated.h"

UCLASS()
class SECRETSOFNORTH_API ASON_Enemy : public ACharacter
{
	GENERATED_BODY()

public:
	ASON_Enemy();

	virtual void SetupParametrs(const FSON_EnemyParameters& EnemyParams);
	bool IsDead() const;

protected:
	UPROPERTY(BlueprintReadOnly)
	FSON_EnemyParameters Params;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxHP = 2.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class ASON_Resource> DropResourceClass;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int DropResourceAmount = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USON_HealthSystem* HealthSystem;

	UPROPERTY(EditDefaultsOnly)
	TArray<UAnimMontage*> DeathMontageArr;
	UPROPERTY(EditDefaultsOnly)
	float DeathFallSpeed = 100;
	UPROPERTY(EditDefaultsOnly)
	float DeathDestroyTime = 3;

	FTimerHandle StartFallTimer;
	FTimerHandle DestroyTimer;

	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;
	
	UFUNCTION(BlueprintCallable)
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

	UFUNCTION()
	virtual void Death();
	UFUNCTION()
	void OnDeathMontageEnd();

	void Destroy();

	void Tick(float DeltaTime) override;
};

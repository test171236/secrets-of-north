// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SON_Enemy.h"
#include "../Common/SON_DamageTypes.h"
#include "SON_Rekken.generated.h"

UCLASS()
class SECRETSOFNORTH_API ASON_Rekken : public ASON_Enemy
{
	GENERATED_BODY()
	
	bool isAttacking = false;

	UPROPERTY()
	class ASON_RekkenController* RekkenController;

protected:
	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//bool isMeleeAttackDebud = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MeleeDamage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSON_DamageInfo MeleeDamageInfo;

	//UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (MakeEditWidget))
	//FVector MeleeAttackOffset;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector MeleeAttackJumpVec;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MeleeAttackRadius;
	UPROPERTY(EditDefaultsOnly)
	UAnimMontage* AttackMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float AgroRadius;

	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type Reason) override;

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void MeleeAttackWindowStart(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload);
	UFUNCTION()
	void MeleeAttackWindowEnd(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload);
	UFUNCTION()
	void MeleeAttackEnd(UAnimMontage* Montage, bool bInterrupted);

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class USoundBase* Sound;

	virtual void SetupParametrs(const FSON_EnemyParameters& EnemyParams) override;

	float GetAttackRadius() const { return MeleeAttackRadius; }
	float GetAgroRadius() const { return AgroRadius; }
	void SetRekkenController(class ASON_RekkenController* newRekkenController) { RekkenController = newRekkenController; }

	UFUNCTION(BlueprintCallable)
	void MeleeAttack(AActor* Target);

};

// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_TestEnemyPawn.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "AITypes.h"
#include "NavigationSystem.h"
#include "AIController.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/Character.h"

// Sets default values
ASON_TestEnemyPawn::ASON_TestEnemyPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Capsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	Capsule->SetCapsuleSize(42.f, 96.0f);
	Capsule->SetCollisionProfileName(FName("Enemy"));
	SetRootComponent(Capsule);

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMesh->SetCollisionProfileName(FName("NoCollision"));
	SkeletalMesh->SetupAttachment(Capsule);

}

void ASON_TestEnemyPawn::FindPath()
{
	ACharacter* ch = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	UNavigationSystemV1* Nav = UNavigationSystemV1::GetCurrent(GetWorld());
	AAIController* cont = Cast<AAIController>(GetController());
	if (ch)
	{
		if (Nav) {
			FPathFindingQuery Query;
			FAIMoveRequest MoveReq;
			MoveReq.SetGoalActor(ch);

			cont->BuildPathfindingQuery(MoveReq, Query);
			FPathFindingResult PathFindingResult = Nav->FindPathSync(Query);
			if (PathFindingResult.IsSuccessful() && PathFindingResult.Path->IsValid()) {
				Path.Empty();
				for (auto& PathPoint : PathFindingResult.Path->GetPathPoints()) {
					Path.Add(PathPoint.Location);
				}
			}
		}
	}
}

// Called when the game starts or when spawned
void ASON_TestEnemyPawn::BeginPlay()
{
	Super::BeginPlay();
	
	GetWorld()->GetTimerManager().SetTimer(
	PathFindingTimer,
	this,
	&ASON_TestEnemyPawn::FindPath,
	1.f / PathFindingRate,
	true);
}

void ASON_TestEnemyPawn::EndPlay(EEndPlayReason::Type Reason)
{
	GetWorld()->GetTimerManager().ClearTimer(PathFindingTimer);
	
	Super::EndPlay(Reason);
}

// Called every frame
void ASON_TestEnemyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Path.Num() >= 2)
	{
		FVector dir = (Path[1] - Path[0]).GetSafeNormal();
		SetActorRotation(dir.Rotation());
		AddActorWorldOffset(dir * MaxSpeed * DeltaTime);
	}
}

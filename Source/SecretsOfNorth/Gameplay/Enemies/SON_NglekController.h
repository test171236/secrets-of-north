// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "SON_NglekController.generated.h"

/**
 * 
 */
UCLASS()
class SECRETSOFNORTH_API ASON_NglekController : public AAIController
{
	GENERATED_BODY()

	UPROPERTY()
	AActor* Target;
	UPROPERTY()
	class ASON_Nglek* Nglek;

	FTimerHandle IdleTimer;

protected:
	virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;
	virtual void OnPossess(APawn* InPawn) override;
	virtual void Tick(float Delta) override;
public:
	ASON_NglekController();
	void FindTarget();

};

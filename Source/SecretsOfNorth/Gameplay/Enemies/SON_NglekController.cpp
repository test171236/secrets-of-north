// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_NglekController.h"
#include "Kismet/GameplayStatics.h"
#include "Navigation/PathFollowingComponent.h"
#include "../../Game/SON_AutobattleGameMode.h"
#include "../Character/SON_PlayerCharacter.h"
#include "../Towers/SON_MainTree.h"
#include "../Towers/SON_LesserTree.h"
#include "../Towers/Soul/SON_Soul.h"
#include "SON_Nglek.h"

ASON_NglekController::ASON_NglekController()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.TickInterval = 0.1;
}

void ASON_NglekController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	if (Result.IsFailure() || !IsValid(Target))
		GetWorld()->GetTimerManager().SetTimer(
			IdleTimer,
			this,
			&ASON_NglekController::FindTarget,
			1.f);
	else
	{
		if(FMath::RandBool())
			Nglek->AOEAttack(Target);
		else
			Nglek->MeleeAttack(Target);
	}

}

void ASON_NglekController::OnPossess(APawn* InPawn)
{
	Nglek = Cast<ASON_Nglek>(InPawn);
	if (!IsValid(Nglek))
		return;

	Super::OnPossess(InPawn);
	Nglek->SetNglekController(this);

	FindTarget();
}

void ASON_NglekController::Tick(float Delta)
{
	Super::Tick(Delta);

	if (Nglek->GetIsAttacking())
		return;

	ASON_AutobattleGameMode* game_mode = Cast<ASON_AutobattleGameMode>(
		UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(game_mode) && IsValid(game_mode->GetPlayerCharacter()))
		if (Target != game_mode->GetPlayerCharacter())
		{
			if (Nglek->GetDistanceTo(game_mode->GetPlayerCharacter()) <= Nglek->GetAgroRadius())
			{
				Target = Cast<AActor>(game_mode->GetPlayerCharacter());
				MoveToActor(Target, Nglek->GetAttackRadius());
			}
		}
		else
		{
			if (Nglek->GetDistanceTo(game_mode->GetPlayerCharacter()) > Nglek->GetAgroRadius())
			{
				Target = nullptr;
				FindTarget();
			}
		}
}

void ASON_NglekController::FindTarget()
{
	if (Nglek->GetIsAttacking())
		return;
	
	{
		ASON_MainTree* tree = Cast<ASON_MainTree>(Target);
		if (IsValid(tree) && tree->GetIsInvincible())
			Target = nullptr;
	}
	{
		ASON_LesserTree* tree = Cast<ASON_LesserTree>(Target);
		if (IsValid(tree) && tree->GetIsDead())
			Target = nullptr;
	}

	if (!IsValid(Target))
	{
		ASON_AutobattleGameMode* game_mode = Cast<ASON_AutobattleGameMode>(
			UGameplayStatics::GetGameMode(GetWorld()));
		if (IsValid(game_mode))
			if (IsValid(game_mode->GetMainTree()) && !game_mode->GetMainTree()->GetIsInvincible())
				Target = Cast<AActor>(game_mode->GetMainTree());
			else
			{
				TArray<AActor*> possible_targets;

				for (auto& tree : game_mode->GetAllLesserTree())
					if (!tree->GetIsDead())
						possible_targets.Add(tree);

				/*if (IsValid(game_mode->GetSoul()) && !game_mode->GetIsSoulInvincible())
					possible_targets.Add(game_mode->GetSoul());*/

				if (possible_targets.Num() > 0)
				{
					int rand = FMath::Rand() % possible_targets.Num();
					Target = possible_targets[rand];
				}
				else
					Target = Cast<AActor>(game_mode->GetPlayerCharacter());
			}
	}

	if (IsValid(Target))
		MoveToActor(Target, Nglek->GetAttackRadius());
	else
		GetWorld()->GetTimerManager().SetTimer(
			IdleTimer,
			this,
			&ASON_NglekController::FindTarget,
			1.f);
}


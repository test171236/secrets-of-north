// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_Abas.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SphereComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "SON_AbasController.h"
#include "../Common/SON_Projectile.h"
#include "../Common/SON_HealthSystem.h"

ASON_Abas::ASON_Abas()
{
	GetMesh()->SetCollisionProfileName("NoCollision");

	NiagaraComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("NiagaraComponent"));
	NiagaraComponent->SetupAttachment(GetMesh(), FName("RightHand"));
	NiagaraComponent->SetVisibility(false);
}

void ASON_Abas::SetupParametrs(const FSON_EnemyParameters& EnemyParams)
{
	Super::SetupParametrs(EnemyParams);

	Damage *= EnemyParams.Damage_Scale;
}

void ASON_Abas::BeginPlay()
{
	Super::BeginPlay();

	/*NiagaraComponent = UNiagaraFunctionLibrary::SpawnSystemAttached(
		VFX,
		Cast<USceneComponent>(GetMesh()),
		FName(""),
		FVector(),
		FRotator(),
		EAttachLocation::SnapToTarget,
		false);*/
	//NiagaraComponent->

	HealthSystem->OnDied.AddDynamic(this, &ASON_Abas::HideProjectile);

	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (IsValid(AnimInstance))
	{
		AnimInstance->OnMontageEnded.AddDynamic(this, &ASON_Abas::AttackEnd);
		AnimInstance->OnPlayMontageNotifyBegin.AddDynamic(this, &ASON_Abas::AttackNotifyStart);
	}
}

void ASON_Abas::EndPlay(EEndPlayReason::Type Reason)
{
	//NiagaraComponent->DestroyComponent();

	Super::EndPlay(Reason);
}

void ASON_Abas::RangeAttack(AActor* Target)
{
	if (!isAttacking)
	{
		isAttacking = true;
		AbasController->StopMovement();
		if (IsValid(Target))
		{
			SetActorRotation((Target->GetActorLocation() - GetActorLocation()).ToOrientationQuat());
			CurrentTarget = Target;
		}
		PlayAnimMontage(AttackMontage);

		UGameplayStatics::PlaySoundAtLocation(GetWorld(),
			Sound,
			GetActorLocation());
	}
}

void ASON_Abas::AttackNotifyStart(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
	if (NotifyName == FName("ProjectileMesh"))
	{
		NiagaraComponent->SetVisibility(true);
	}
	else if (NotifyName == FName("SpawnProjectile"))
	{
		NiagaraComponent->SetVisibility(false);

		FVector dir = CurrentTarget->GetActorLocation() - NiagaraComponent->GetComponentLocation();
		dir = dir.GetSafeNormal();

		ASON_Projectile* proj = GetWorld()->SpawnActorDeferred<ASON_Projectile>(
			ProjectileClass,
			FTransform(dir.Rotation(), NiagaraComponent->GetComponentLocation(), FVector(1.f)),
			GetOwner(),
			nullptr,
			ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
		proj->SetParams(Damage);
		proj->CollisionComp->SetCollisionProfileName(TEXT("EnemyProjectile"));
		proj->FinishSpawning(FTransform(dir.Rotation(), NiagaraComponent->GetComponentLocation(), FVector(1.f)));

	}
}

void ASON_Abas::AttackEnd(UAnimMontage* Montage, bool bInterrupted)
{
	if (Montage == AttackMontage)
	{
		isAttacking = false;
		if (IsValid(AbasController))
			AbasController->FindTarget();
	}
}

void ASON_Abas::HideProjectile()
{
	NiagaraComponent->SetVisibility(false);
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_EnemySpawnPoint.h"
#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"
#include "SON_Enemy.h"

ASON_EnemySpawnPoint::ASON_EnemySpawnPoint()
{
	PrimaryActorTick.bCanEverTick = false;
}

bool ASON_EnemySpawnPoint::SpawnEnemy(const FSON_EnemyParameters& EnemyParams)
{
	FHitResult hr;

	UKismetSystemLibrary::LineTraceSingle(
		GetWorld(),
		GetActorLocation(),
		GetActorLocation() + FVector(0, 0, -1) * 1e6,
		UEngineTypes::ConvertToTraceType(ECC_GameTraceChannel1),
		false,
		TArray<AActor*>(),
		EDrawDebugTrace::None,
		hr,
		false);

	if (hr.bBlockingHit)
	{
		FVector spawn_loc = hr.ImpactPoint +
			FVector(0, 0, 1) * EnemyParams.EnemyClass.GetDefaultObject()->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();

		ASON_Enemy* enemy = GetWorld()->SpawnActorDeferred<ASON_Enemy>(
			EnemyParams.EnemyClass, 
			FTransform(spawn_loc),
			nullptr,
			nullptr,
			ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding);
		if (!IsValid(enemy))
			return false;

		enemy->SetupParametrs(EnemyParams);
		enemy->FinishSpawning(FTransform(spawn_loc));
	}
	else
		return false;

	return true;
}

void ASON_EnemySpawnPoint::BeginPlay()
{
	Super::BeginPlay();
	
	ASON_AutobattleGameMode* game_mode = Cast<ASON_AutobattleGameMode>(
		UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(game_mode))
		game_mode->AddEnemySpawnPoint(this);

}


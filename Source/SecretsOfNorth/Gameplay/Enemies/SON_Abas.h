// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SON_Enemy.h"
#include "Components/StaticMeshComponent.h"
#include "SON_Abas.generated.h"

/**
 * 
 */
UCLASS()
class SECRETSOFNORTH_API ASON_Abas : public ASON_Enemy
{
	GENERATED_BODY()
	
	bool isAttacking = false;

	UPROPERTY()
	class ASON_AbasController* AbasController;
	UPROPERTY()
	AActor* CurrentTarget;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Damage;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float AttackRadius;
	UPROPERTY(EditDefaultsOnly)
	UAnimMontage* AttackMontage;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class ASON_Projectile> ProjectileClass;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UNiagaraComponent* NiagaraComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float AgroRadius;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float RunAwayRadius;

	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type Reason) override;

	UFUNCTION()
	void AttackNotifyStart(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload);
	UFUNCTION()
	void AttackEnd(UAnimMontage* Montage, bool bInterrupted);
	UFUNCTION()
	void HideProjectile();

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class USoundBase* Sound;
	
	ASON_Abas();
	
	virtual void SetupParametrs(const FSON_EnemyParameters& EnemyParams) override;

	float GetAttackRadius() const { return AttackRadius; }
	float GetAgroRadius() const { return AgroRadius; }
	float GetRunAwayRadius() const { return RunAwayRadius; }
	void SetAbasController(class ASON_AbasController* newAbasController) { AbasController = newAbasController; }
	bool GetIsAttacking() const { return isAttacking; }

	UFUNCTION(BlueprintCallable)
	void RangeAttack(AActor* Target);

};

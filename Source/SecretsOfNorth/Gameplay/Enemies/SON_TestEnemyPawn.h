// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SON_TestEnemyPawn.generated.h"

UCLASS()
class SECRETSOFNORTH_API ASON_TestEnemyPawn : public APawn
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<FVector> Path;
	UPROPERTY()
	FTimerHandle PathFindingTimer;
	UPROPERTY()
	float PathFindingRate = 5;

	UFUNCTION()
	void FindPath();

public:
	// Sets default values for this pawn's properties
	ASON_TestEnemyPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type Reason) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UCapsuleComponent* Capsule;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USkeletalMeshComponent * SkeletalMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxSpeed = 100;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_Enemy.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "../Common/SON_HealthSystem.h"
#include "../Character/SON_Resource.h"
#include "../Towers/SON_MainTree.h"

// Sets default values
ASON_Enemy::ASON_Enemy()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	GetCapsuleComponent()->SetCollisionProfileName(FName("Enemy"));

	GetCharacterMovement()->bOrientRotationToMovement = true; 
	GetCharacterMovement()->RotationRate = FRotator(0.f, 120.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	HealthSystem = CreateDefaultSubobject<USON_HealthSystem>(TEXT("HealthSystem"));

	HealthSystem->OnDied.AddDynamic(this, &ASON_Enemy::Death);
}

void ASON_Enemy::SetupParametrs(const FSON_EnemyParameters& EnemyParams)
{
	Params = EnemyParams;
	MaxHP *= Params.HP_Scale;
	GetCharacterMovement()->MaxWalkSpeed *= Params.MovementSpeed_Scale;
}

bool ASON_Enemy::IsDead() const
{
	return HealthSystem->GetIsDead();
}

void ASON_Enemy::BeginPlay()
{
	Super::BeginPlay();

	HealthSystem->SetMaxHP(MaxHP);

	ASON_AutobattleGameMode* game_mode = Cast<ASON_AutobattleGameMode>(
		UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(game_mode))
		game_mode->AddEnemy(this);

	/*UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (IsValid(AnimInstance))
	{
		AnimInstance->OnMontageEnded.AddDynamic(this, &ASON_Enemy::OnDeathMontageEnd);
	}*/
}

void ASON_Enemy::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	ASON_AutobattleGameMode* game_mode = Cast<ASON_AutobattleGameMode>(
		UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(game_mode))
		game_mode->RemoveEnemy(this);

	Super::EndPlay(EndPlayReason);
}

float ASON_Enemy::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float res = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	
	HealthSystem->ReduceHP(res);

	return res;
}

void ASON_Enemy::Death()
{
	for (int i = 0; i < DropResourceAmount; i++)
		GetWorld()->SpawnActor<ASON_Resource>(DropResourceClass, FTransform(GetActorLocation()));

	GetController()->StopMovement();
	GetController()->Destroy();
	GetCharacterMovement()->SetComponentTickEnabled(false);
	GetCapsuleComponent()->SetCollisionProfileName(FName("NoCollision"));

	UAnimMontage* DeathMontage = DeathMontageArr[FMath::Rand() % DeathMontageArr.Num()];

	GetWorld()->GetTimerManager().SetTimer(StartFallTimer,
		this,
		&ASON_Enemy::OnDeathMontageEnd,
		DeathMontage->GetPlayLength());
	PlayAnimMontage(DeathMontage);
}

void ASON_Enemy::OnDeathMontageEnd()
{
	GetWorld()->GetTimerManager().SetTimer(
		DestroyTimer,
		this,
		&ASON_Enemy::Destroy,
		DeathDestroyTime);
	SetActorTickEnabled(true);
}

void ASON_Enemy::Destroy()
{
	Super::Destroy();
}

void ASON_Enemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HealthSystem->GetIsDead())
	{
		AddActorWorldOffset(DeathFallSpeed * DeltaTime * FVector(0,0,-1));
	}
}


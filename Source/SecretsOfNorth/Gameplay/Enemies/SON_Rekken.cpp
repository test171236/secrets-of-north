// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_Rekken.h"
#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"
#include "SON_RekkenController.h"

void ASON_Rekken::BeginPlay()
{
	Super::BeginPlay();

	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ASON_Rekken::OnBeginOverlap);

	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (IsValid(AnimInstance))
	{
		AnimInstance->OnMontageEnded.AddDynamic(this, &ASON_Rekken::MeleeAttackEnd);
		AnimInstance->OnPlayMontageNotifyBegin.AddDynamic(this, &ASON_Rekken::MeleeAttackWindowStart);
		AnimInstance->OnPlayMontageNotifyEnd.AddDynamic(this, &ASON_Rekken::MeleeAttackWindowEnd);
	}
}

void ASON_Rekken::EndPlay(EEndPlayReason::Type Reason)
{
	Super::EndPlay(Reason);
}

void ASON_Rekken::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (IsValid(OtherActor) && isAttacking)
		OtherActor->TakeDamage(MeleeDamage, MeleeDamageInfo, RekkenController, this);
}

void ASON_Rekken::MeleeAttackWindowStart(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
	if (NotifyName != FName("MeleeAttack"))
		return;

	GetCapsuleComponent()->SetCollisionProfileName(TEXT("EnemyMeleeAttack1"));
	LaunchCharacter(GetActorRotation().RotateVector(MeleeAttackJumpVec), true, true);
	
	//FVector center = GetActorLocation() + GetActorRotation().RotateVector(MeleeAttackOffset);

	/*if (isMeleeAttackDebud)
		DrawDebugSphere(GetWorld(),
			center,
			MeleeAttackRadius,
			16,
			FColor::Red,
			false,
			1.f);

	TArray<AActor*> out;

	if (UKismetSystemLibrary::SphereOverlapActors(GetWorld(),
		center,
		MeleeAttackRadius,
		TArray<TEnumAsByte<EObjectTypeQuery>>({
			UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel3),
			UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel5) }),
			nullptr,
			TArray<AActor*>(),
			out))
			for (auto i : out)
			{
				i->TakeDamage(MeleeDamage, MeleeDamageInfo, GetController(), this);
			}  */
}

void ASON_Rekken::MeleeAttackWindowEnd(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
	if (NotifyName != FName("MeleeAttack"))
		return;
	
	GetCapsuleComponent()->SetCollisionProfileName(TEXT("Enemy"));
}

void ASON_Rekken::MeleeAttack(AActor* Target)
{
	if (!isAttacking)
	{
		isAttacking = true;
		if (IsValid(Target))
			SetActorRotation((Target->GetActorLocation() - GetActorLocation()).ToOrientationQuat());

		PlayAnimMontage(AttackMontage);
	}
}

void ASON_Rekken::MeleeAttackEnd(UAnimMontage* Montage, bool bInterrupted)
{
	if (Montage == AttackMontage)
	{
		isAttacking = false;
		if (IsValid(RekkenController))
			RekkenController->FindTarget();
	}
}

void ASON_Rekken::SetupParametrs(const FSON_EnemyParameters& EnemyParams)
{
	Super::SetupParametrs(EnemyParams);

	MeleeDamage *= EnemyParams.Damage_Scale;
}

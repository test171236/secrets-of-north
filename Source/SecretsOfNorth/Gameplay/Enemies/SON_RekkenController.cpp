// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_RekkenController.h"
#include "Kismet/GameplayStatics.h"
#include "Navigation/PathFollowingComponent.h"
#include "../../Game/SON_AutobattleGameMode.h"
#include "../Character/SON_PlayerCharacter.h"
#include "../Towers/SON_MainTree.h"
#include "../Towers/SON_LesserTree.h"
#include "../Towers/Soul/SON_Soul.h"
#include "SON_Rekken.h"

ASON_RekkenController::ASON_RekkenController()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.TickInterval = 0.1;
}

void ASON_RekkenController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	if (Result.IsFailure() || !IsValid(Target))
		GetWorld()->GetTimerManager().SetTimer(
			IdleTimer,
			this,
			&ASON_RekkenController::FindTarget,
			1.f);
	else
	{
		Rekken->MeleeAttack(Target);
	}

}

void ASON_RekkenController::OnPossess(APawn* InPawn)
{
	Rekken = Cast<ASON_Rekken>(InPawn);
	if (!IsValid(Rekken))
		return;

	Super::OnPossess(InPawn);
	Rekken->SetRekkenController(this);

	FindTarget();
}

void ASON_RekkenController::Tick(float Delta)
{
	Super::Tick(Delta);

	ASON_AutobattleGameMode* game_mode = Cast<ASON_AutobattleGameMode>(
		UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(game_mode) && IsValid(game_mode->GetPlayerCharacter()))
		if (Target != game_mode->GetPlayerCharacter())
		{
			if (Rekken->GetDistanceTo(game_mode->GetPlayerCharacter()) <= Rekken->GetAgroRadius())
			{
				Target = Cast<AActor>(game_mode->GetPlayerCharacter());
				MoveToActor(Target, Rekken->GetAttackRadius());
			}
		}
		else
		{
			if (Rekken->GetDistanceTo(game_mode->GetPlayerCharacter()) > Rekken->GetAgroRadius())
			{
				Target = nullptr;
				FindTarget();
			}
		}

	if (FMath::FRand() < 0.17)
		UGameplayStatics::PlaySoundAtLocation(GetWorld(),
			Rekken->Sound, 
			Rekken->GetActorLocation());
}

void ASON_RekkenController::FindTarget()
{
	{
		ASON_MainTree* tree = Cast<ASON_MainTree>(Target);
		if (IsValid(tree) && tree->GetIsInvincible())
			Target = nullptr;
	}
	{
		ASON_LesserTree* tree = Cast<ASON_LesserTree>(Target);
		if (IsValid(tree) && tree->GetIsDead())
			Target = nullptr;
	}

	if (!IsValid(Target))
	{
		ASON_AutobattleGameMode* game_mode = Cast<ASON_AutobattleGameMode>(
			UGameplayStatics::GetGameMode(GetWorld()));
		if (IsValid(game_mode))
			if (IsValid(game_mode->GetMainTree()) && !game_mode->GetMainTree()->GetIsInvincible())
				Target = Cast<AActor>(game_mode->GetMainTree());
			else
			{
				TArray<AActor*> possible_targets;

				for (auto& tree : game_mode->GetAllLesserTree())
					if (!tree->GetIsDead())
						possible_targets.Add(tree);

				/*if (IsValid(game_mode->GetSoul()) && !game_mode->GetIsSoulInvincible())
					possible_targets.Add(game_mode->GetSoul());	*/

				if (possible_targets.Num() > 0)
				{
					int rand = FMath::Rand() % possible_targets.Num();
					Target = possible_targets[rand];
				}
				else
					Target = Cast<AActor>(game_mode->GetPlayerCharacter());
			}
	}

	if (IsValid(Target))
		MoveToActor(Target, Rekken->GetAttackRadius());
	else
		GetWorld()->GetTimerManager().SetTimer(
			IdleTimer,
			this,
			&ASON_RekkenController::FindTarget,
			1.f);
}

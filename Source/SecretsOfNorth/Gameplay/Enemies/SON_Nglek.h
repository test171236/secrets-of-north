// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SON_Enemy.h"
#include "SON_Nglek.generated.h"

/**
 * 
 */
UCLASS()
class SECRETSOFNORTH_API ASON_Nglek : public ASON_Enemy
{
	GENERATED_BODY()
	
	bool isAttacking = false;
	bool canAttack = true;
	float DashTickDelta = 0.01;
	FTimerHandle DashTickTimer;

	UPROPERTY()
	class ASON_NglekController* NglekController;

	UFUNCTION()
	void DashTick();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MeleeDamage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AOEDamage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSON_DamageInfo DamageInfo;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MeeleAttackSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MeleeAttackRadius;
	UPROPERTY(EditDefaultsOnly)
	UAnimMontage* MeleeAttackMontage;
	UPROPERTY(EditDefaultsOnly)
	UAnimMontage* AOEAttackMontage;
	UPROPERTY(EditDefaultsOnly)
	float AOEAttackRadius;
	UPROPERTY(EditDefaultsOnly)
	class UNiagaraSystem* AOEParticleSystem;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float AgroRadius;

	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type Reason) override;

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void AttackWindowStart(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload);
	UFUNCTION()
	void AttackWindowEnd(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload);
	UFUNCTION()
	void AttackEnd(UAnimMontage* Montage, bool bInterrupted);

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class USoundBase* Sound;

	virtual void SetupParametrs(const FSON_EnemyParameters& EnemyParams) override;

	float GetAttackRadius() const { return MeleeAttackRadius; }
	float GetAgroRadius() const { return AgroRadius; }
	void SetNglekController(class ASON_NglekController* newNglekController) { NglekController = newNglekController; }

	bool GetIsAttacking() const { return isAttacking; }

	UFUNCTION(BlueprintCallable)
	void MeleeAttack(AActor* Target);
	UFUNCTION(BlueprintCallable)
	void AOEAttack(AActor* Target);

};

// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_AbasController.h"
#include "Kismet/GameplayStatics.h"
#include "Navigation/PathFollowingComponent.h"
#include "Components/CapsuleComponent.h"
#include "../../Game/SON_AutobattleGameMode.h"
#include "../Character/SON_PlayerCharacter.h"
#include "../Towers/SON_MainTree.h"
#include "../Towers/SON_LesserTree.h"
#include "../Towers/Soul/SON_Soul.h"
#include "SON_Abas.h"

ASON_AbasController::ASON_AbasController()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.TickInterval = 0.1;
}

void ASON_AbasController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	if (Result.IsFailure() || !IsValid(Target))
		GetWorld()->GetTimerManager().SetTimer(
			IdleTimer,
			this,
			&ASON_AbasController::FindTarget,
			1.f);
	else if (Result.IsSuccess())
	{
		float dist = Abas->GetDistanceTo(Target);
		if (Abas->GetRunAwayRadius() < dist && dist < Abas->GetAgroRadius())
			Abas->RangeAttack(Target);
		else
			FindTarget();
	}

}

void ASON_AbasController::OnPossess(APawn* InPawn)
{
	Abas = Cast<ASON_Abas>(InPawn);
	if (!IsValid(Abas))
		return;

	Super::OnPossess(InPawn);
	Abas->SetAbasController(this);

	FindTarget();
}

void ASON_AbasController::Tick(float Delta)
{
	Super::Tick(Delta);

	ASON_AutobattleGameMode* game_mode = Cast<ASON_AutobattleGameMode>(
		UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(game_mode) && IsValid(game_mode->GetPlayerCharacter()))
		if (Target != game_mode->GetPlayerCharacter())
		{
			if (Abas->GetDistanceTo(game_mode->GetPlayerCharacter()) <= Abas->GetAgroRadius())
			{
				Target = Cast<AActor>(game_mode->GetPlayerCharacter());
				if (!Abas->GetIsAttacking())
					MoveToActor(Target, Abas->GetAttackRadius());
			}
		}
		else
		{
			if (Abas->GetDistanceTo(game_mode->GetPlayerCharacter()) > Abas->GetAgroRadius())
			{
				Target = nullptr;
				FindTarget();
			}
		}
}

void ASON_AbasController::RunAway()
{
	if (Abas->GetIsAttacking())
		return;

	FVector dir = Abas->GetActorLocation() - Target->GetActorLocation();
	dir = FVector(dir.X, dir.Y, 0).GetSafeNormal();
	float len = Abas->GetAttackRadius() - (Abas->GetActorLocation() - Target->GetActorLocation()).Length();
	FVector loc = Abas->GetActorLocation() + dir * len;

	FHitResult out_hit;

	UKismetSystemLibrary::CapsuleTraceSingleForObjects(
		GetWorld(),
		Abas->GetActorLocation(),
		loc,
		Abas->GetCapsuleComponent()->GetScaledCapsuleRadius(),
		Abas->GetCapsuleComponent()->GetScaledCapsuleHalfHeight(),
		TArray<TEnumAsByte<EObjectTypeQuery>>({
			UEngineTypes::ConvertToObjectType(ECC_WorldStatic),
			UEngineTypes::ConvertToObjectType(ECC_WorldDynamic),
			UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel5)}),
		false,
		TArray<AActor*>({ Abas }),
		EDrawDebugTrace::None,
		out_hit,
		true);

	if (out_hit.bBlockingHit)
		loc = out_hit.Location;

	if ((loc - Abas->GetActorLocation()).Length() > len/2)
		MoveToLocation(loc, 10);
	else
		Abas->RangeAttack(Target);
}

void ASON_AbasController::FindTarget()
{
	if (Abas->GetIsAttacking())
		return;

	{
		ASON_MainTree* tree = Cast<ASON_MainTree>(Target);
		if (IsValid(tree) && tree->GetIsInvincible())
			Target = nullptr;
	}
	{
		ASON_LesserTree* tree = Cast<ASON_LesserTree>(Target);
		if (IsValid(tree) && tree->GetIsDead())
			Target = nullptr;
	}

	if (!IsValid(Target))
	{
		ASON_AutobattleGameMode* game_mode = Cast<ASON_AutobattleGameMode>(
			UGameplayStatics::GetGameMode(GetWorld()));
		if (IsValid(game_mode))
			if (IsValid(game_mode->GetMainTree()) && !game_mode->GetMainTree()->GetIsInvincible())
				Target = Cast<AActor>(game_mode->GetMainTree());
			else
			{
				TArray<AActor*> possible_targets;

				for (auto& tree : game_mode->GetAllLesserTree())
					if (!tree->GetIsDead())
						possible_targets.Add(tree);

				/*if (IsValid(game_mode->GetSoul()) && !game_mode->GetIsSoulInvincible())
					possible_targets.Add(game_mode->GetSoul());*/
				
				if (possible_targets.Num() > 0)
				{
					int rand = FMath::Rand() % possible_targets.Num();
					Target = possible_targets[rand];
				}
				else
					Target = Cast<AActor>(game_mode->GetPlayerCharacter());
			}
	}

	if (IsValid(Target))
	{
		float dist = Abas->GetDistanceTo(Target);
		if (dist > Abas->GetRunAwayRadius())
			MoveToActor(Target, Abas->GetAttackRadius());
		else
			RunAway();
	}
	else
		GetWorld()->GetTimerManager().SetTimer(
			IdleTimer,
			this,
			&ASON_AbasController::FindTarget,
			1.f);
}



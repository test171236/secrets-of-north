// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_Nglek.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/CapsuleComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "SON_NglekController.h"

void ASON_Nglek::BeginPlay()
{
	Super::BeginPlay();

	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ASON_Nglek::OnBeginOverlap);

	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (IsValid(AnimInstance))
	{
		AnimInstance->OnMontageEnded.AddDynamic(this, &ASON_Nglek::AttackEnd);
		AnimInstance->OnPlayMontageNotifyBegin.AddDynamic(this, &ASON_Nglek::AttackWindowStart);
		AnimInstance->OnPlayMontageNotifyEnd.AddDynamic(this, &ASON_Nglek::AttackWindowEnd);
	}
}

void ASON_Nglek::EndPlay(EEndPlayReason::Type Reason)
{
	Super::EndPlay(Reason);
}

void ASON_Nglek::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (IsValid(OtherActor) && isAttacking && canAttack)
	{
		OtherActor->TakeDamage(MeleeDamage, DamageInfo, NglekController, this);
		canAttack = false;
	}
}

void ASON_Nglek::DashTick()
{
	AddActorLocalOffset(FVector(MeeleAttackSpeed * DashTickDelta, 0, 0), true);
}									  

void ASON_Nglek::AttackWindowStart(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
	if (NotifyName == FName("MeleeAttack"))
	{
		canAttack = true;
		GetCapsuleComponent()->SetCollisionProfileName(TEXT("EnemyMeleeAttack1"));
		//LaunchCharacter(GetActorRotation().RotateVector(MeleeAttackJumpVec), true, true);
		GetWorld()->GetTimerManager().SetTimer(
			DashTickTimer,
			this,
			&ASON_Nglek::DashTick,
			DashTickDelta,
			true);
	}
	else if (NotifyName == FName("AOEAttack"))
	{
		//RHandSocket
		FVector loc = GetMesh()->GetSocketLocation(TEXT("RHandSocket"));
		FHitResult hr;

		if (UKismetSystemLibrary::LineTraceSingle(GetWorld(),
			loc,
			loc + FVector(0, 0, -1e6),
			UEngineTypes::ConvertToTraceType(ECC_GameTraceChannel1),
			false,
			TArray<AActor*>(),
			EDrawDebugTrace::None,
			hr,
			true))
		{
			FVector center = hr.ImpactPoint + FVector(0,0,1);

			UNiagaraComponent* comp = UNiagaraFunctionLibrary::SpawnSystemAtLocation(
				GetWorld(),
				AOEParticleSystem,
				center, 
				FRotator::ZeroRotator,
				FVector(1.f),
				true,
				false);

			comp->SetFloatParameter(TEXT("User.Radius"), AOEAttackRadius * 1.5);
			comp->Activate(true);

			TArray<AActor*> out;
			UKismetSystemLibrary::SphereOverlapActors(GetWorld(),
				center,
				AOEAttackRadius,
				TArray<TEnumAsByte<EObjectTypeQuery>>({
					UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel3),
					UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel5) }),
				nullptr,
				TArray<AActor*>(),
				out);

			for (auto& actor : out)
				actor->TakeDamage(AOEDamage, DamageInfo, NglekController, this);
		}
	}
}

void ASON_Nglek::AttackWindowEnd(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
	if (NotifyName == FName("MeleeAttack"))
	{
		GetCapsuleComponent()->SetCollisionProfileName(TEXT("Enemy"));
		GetWorld()->GetTimerManager().ClearTimer(DashTickTimer);
	}
}

void ASON_Nglek::AttackEnd(UAnimMontage* Montage, bool bInterrupted)
{
	if (Montage == MeleeAttackMontage || Montage == AOEAttackMontage)
	{
		isAttacking = false;
		if (IsValid(NglekController))
			NglekController->FindTarget();
	}
}

void ASON_Nglek::SetupParametrs(const FSON_EnemyParameters& EnemyParams)
{
	Super::SetupParametrs(EnemyParams);

	MeleeDamage *= EnemyParams.Damage_Scale;
	AOEDamage *= EnemyParams.Damage_Scale;
}

void ASON_Nglek::MeleeAttack(AActor* Target)
{		  
	if (!isAttacking)
	{
		isAttacking = true;
		if (IsValid(Target))
			SetActorRotation((Target->GetActorLocation() - GetActorLocation()).ToOrientationQuat());

		PlayAnimMontage(MeleeAttackMontage);

		UGameplayStatics::PlaySoundAtLocation(GetWorld(),
			Sound,
			GetActorLocation());
	}
}

void ASON_Nglek::AOEAttack(AActor* Target)
{
	if (!isAttacking)
	{
		isAttacking = true;
		if (IsValid(Target))
			SetActorRotation((Target->GetActorLocation() - GetActorLocation()).ToOrientationQuat());

		PlayAnimMontage(AOEAttackMontage);	

		UGameplayStatics::PlaySoundAtLocation(GetWorld(),
			Sound,
			GetActorLocation());
	}
}

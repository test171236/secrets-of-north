#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "Engine/DamageEvents.h"
#include "SON_DamageTypes.generated.h"

USTRUCT(BlueprintType)
struct SECRETSOFNORTH_API FSON_DamageInfo : public FDamageEvent
{
	GENERATED_BODY()

};

UCLASS()
class SECRETSOFNORTH_API USON_ProjectileDamage : public UDamageType
{
	GENERATED_BODY()
	
};

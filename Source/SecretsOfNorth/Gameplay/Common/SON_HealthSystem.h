// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SON_HealthSystem.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SECRETSOFNORTH_API USON_HealthSystem : public UActorComponent
{
	GENERATED_BODY()

	float MaxHP = 100.f;
	float CurHP = 100.f;
	bool isDead = false;

public:	
	USON_HealthSystem();

protected:
	virtual void BeginPlay() override;

public:	
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDied);
	UPROPERTY(BlueprintAssignable)
	FOnDied	OnDied;

	UFUNCTION(BlueprintCallable)
	void SetMaxHP(float newMaxHP);

	UFUNCTION(BlueprintCallable)
	void ChangeMaxHP(float newMaxHP);

	UFUNCTION(BlueprintCallable)
	void SetCurrentHP(float curHP);

	UFUNCTION(BlueprintCallable)
	float ReduceHP(float deltaHP);

	UFUNCTION(BlueprintCallable)
	float AddHP(float deltaHP);

	UFUNCTION(BlueprintCallable)
	void RestoreFullHP();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCurHP() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetMaxHP() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetIsDead() const;

};

// Fill out your copyright notice in the Description page of Project Settings.

#include "SON_BasicAbility_Beam.h"

#include "Kismet/KismetSystemLibrary.h"
#include "SON_Beam.h"
#include "../../Enemies/SON_Enemy.h"

USON_BasicAbility_Beam::USON_BasicAbility_Beam()
{
	PrimaryComponentTick.bCanEverTick = true;
	//PrimaryComponentTick.TickInterval = 0.1f;
	PrimaryComponentTick.bStartWithTickEnabled = false;

	ObjectChannels.Add(UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel4));
	TargetClasses.Add(ASON_Enemy::StaticClass());
}

void USON_BasicAbility_Beam::BeginPlay()
{
	Super::BeginPlay();

	Beam = GetWorld()->SpawnActor<ASON_Beam>(BaseParametrs.SpawnableClass);
	Beam->SetActorHiddenInGame(true);
	Beam->SetupActors(GetOwner(), nullptr);
}

void USON_BasicAbility_Beam::DeactivateAbility()
{
	State = ESON_AbilityState::Ready;
	SetComponentTickEnabled(false);
}

void USON_BasicAbility_Beam::ActivateAbility()
{
	if (State != ESON_AbilityState::Ready)
		return;

	State = ESON_AbilityState::Active;

	SetComponentTickEnabled(true);
}

void USON_BasicAbility_Beam::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (State == ESON_AbilityState::Active && IsValid(GetOwner()))
	{
		TArray<AActor*> out;
		AActor* target = nullptr;
		float target_distance = 1e10;

		if (UKismetSystemLibrary::SphereOverlapActors(GetWorld(),
			GetOwner()->GetActorLocation(),
			GetAbilityRange(),
			ObjectChannels,
			NULL,
			TArray<AActor*>(),
			out))
		{
			TArray<AActor*> out_filtred;

			for (auto& act : out)
				for (auto& cl : TargetClasses)
					if (act->IsA(cl))
						out_filtred.AddUnique(act);

			for (auto& i : out_filtred)
			{
				float dist = GetOwner()->GetDistanceTo(i);
				if (target == nullptr || dist < target_distance)
				{
					target = i;
					target_distance = dist;
				}
			}

			if (IsValid(target))
			{
				FActorSpawnParameters asp;
				asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				asp.Owner = GetOwner();

				FVector dir = target->GetActorLocation() - GetOwner()->GetActorLocation();
				dir = FVector(dir.X, dir.Y, 0).GetSafeNormal();

				Beam->InitParametrs(BaseParametrs, BonusParametrs);
				Beam->SetupActors(GetOwner(), target);

				if (Beam->IsHidden())
					Beam->SetActorHiddenInGame(false);
			}
			else
				if (!Beam->IsHidden())
					Beam->SetActorHiddenInGame(true);
		}
		else if (!Beam->IsHidden())
			Beam->SetActorHiddenInGame(true);
	}
	else if (!Beam->IsHidden())
		Beam->SetActorHiddenInGame(true);
}
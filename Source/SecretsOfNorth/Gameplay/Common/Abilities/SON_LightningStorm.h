// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SON_PlayerAbility.h"
#include "SON_LightningStorm.generated.h"

/**
 * 
 */
UCLASS()
class SECRETSOFNORTH_API USON_LightningStorm : public USON_PlayerAbility
{
	GENERATED_BODY()
	
	FTimerHandle FireTimer;
	void Fire();

	void DeactivateAbility() override;
	void CooldownEnd();
public:
	virtual void ActivateAbility() override;

};

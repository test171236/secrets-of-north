// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_Beam.h"

#include "../SON_DamageTypes.h"
#include "NiagaraComponent.h"

ASON_Beam::ASON_Beam()
{
    VFX = CreateDefaultSubobject<UNiagaraComponent>("VFX");
}

void ASON_Beam::SetupActors(AActor* newActorBegin, AActor* newActorEnd)
{
    ActorBegin = newActorBegin, ActorEnd = newActorEnd;

    if (IsValid(ActorBegin) && IsValid(ActorEnd))
    {
        SetActorLocation(ActorBegin->GetActorLocation());
        SetActorRotation((ActorEnd->GetActorLocation() - ActorBegin->GetActorLocation()).Rotation());

        VFX->SetFloatParameter(TEXT("Lenght"), (ActorEnd->GetActorLocation() - ActorBegin->GetActorLocation()).Length());
    }
}

void ASON_Beam::BeginPlay()
{
    Super::BeginPlay();
}

void ASON_Beam::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (IsValid(ActorBegin) && IsValid(ActorEnd))
    {
        SetActorLocation(ActorBegin->GetActorLocation());
        SetActorRotation((ActorEnd->GetActorLocation() - ActorBegin->GetActorLocation()).Rotation());

        VFX->SetFloatParameter(TEXT("Lenght"), (ActorEnd->GetActorLocation() - ActorBegin->GetActorLocation()).Length());
        ActorEnd->TakeDamage(GetActorDamage() * DeltaTime, FSON_DamageInfo(), nullptr, this);
    }
}

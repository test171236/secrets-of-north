// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_ShootProjectileAbility.h"

#include "Kismet/KismetSystemLibrary.h"
#include "../SON_Projectile.h"
#include "../../Enemies/SON_Enemy.h"
#include "../../Character/SON_PlayerCharacter.h"

USON_ShootProjectileAbility::USON_ShootProjectileAbility()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.TickInterval = 0.1f;
	PrimaryComponentTick.bStartWithTickEnabled = false;

	ObjectChannels.Add(UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel4)); 
	TargetClasses.Add(ASON_Enemy::StaticClass());
}


void USON_ShootProjectileAbility::BeginPlay()
{
	Super::BeginPlay();
}

void USON_ShootProjectileAbility::DeactivateAbility()
{
	State = ESON_AbilityState::Cooldown;
	SetComponentTickEnabled(false);
	GetWorld()->GetTimerManager().SetTimer(
		CooldownTimer,
		this,
		&USON_ShootProjectileAbility::CooldownEnd,
		BaseParametrs.CooldownTime / (1 + BonusParametrs.CooldownTime));

	/*ASON_PlayerCharacter* player = Cast<ASON_PlayerCharacter>(GetOwner());
	if (IsValid(player))
		player->StopAbilitySound(Name);	*/
}

void USON_ShootProjectileAbility::CooldownEnd()
{
	State = ESON_AbilityState::Ready;
}

void USON_ShootProjectileAbility::ActivateAbility()
{
	if (State != ESON_AbilityState::Ready)
		return;

	State = ESON_AbilityState::Active;

	if (!IsAlwaysActive)
	{
		bool res;
		UWorld* world = GetWorldChecked(res);
		world->GetTimerManager().SetTimer(
			ActivationTimer,
			this,
			&USON_ShootProjectileAbility::DeactivateAbility,
			BaseParametrs.DurationTime * (1 + BonusParametrs.DurationTime));
	}
	SetComponentTickEnabled(true);

	/*ASON_PlayerCharacter* player = Cast<ASON_PlayerCharacter>(GetOwner());
	if (IsValid(player))
		player->PlayAbilitySound(Name);*/
}

void USON_ShootProjectileAbility::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (State == ESON_AbilityState::Active && !IsFiring && IsValid(GetOwner()))
	{
		TArray<AActor*> out;

		if (UKismetSystemLibrary::SphereOverlapActors(GetWorld(),
			GetOwner()->GetActorLocation(),
			BaseParametrs.Range * (1 + BonusParametrs.Range),
			ObjectChannels,
			NULL,
			TArray<AActor*>(),
			out))
		{
			for (auto& act : out)
				for (auto& cl : TargetClasses)
					if (act->IsA(cl))
						IsFiring = true;

			if (IsFiring)
				GetWorld()->GetTimerManager().SetTimer(
					FireTimer,
					this,
					&USON_ShootProjectileAbility::Fire,
					1.f / (BaseParametrs.FireRate * (1 + BonusParametrs.FireRate)),
					true,
					0.f);
		}
	}
}

void USON_ShootProjectileAbility::Fire()
{
	if (State != ESON_AbilityState::Active)
	{
		IsFiring = false;
		GetWorld()->GetTimerManager().ClearTimer(FireTimer);
		return;
	}

	if (IsValid(GetOwner()))
	{
		TArray<AActor*> out;
		AActor* target = nullptr;
		float target_distance = 1e10;

		if (UKismetSystemLibrary::SphereOverlapActors(GetWorld(),
			GetOwner()->GetActorLocation(), 
			BaseParametrs.Range * (1 + BonusParametrs.Range),
			ObjectChannels,
			NULL,
			TArray<AActor*>(),
			out))
		{
			TArray<AActor*> out_filtred;

			for (auto& act : out)
				for (auto& cl : TargetClasses)
					if (act->IsA(cl))
						out_filtred.AddUnique(act);

			for (auto& i : out_filtred)
			{
				float dist = GetOwner()->GetDistanceTo(i);
				if (target == nullptr || dist < target_distance)
				{
					target = i;
					target_distance = dist;
				}
			}

			if (IsValid(target))
			{
				FActorSpawnParameters asp;
				asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				asp.Owner = GetOwner();

				FVector dir = target->GetActorLocation() - GetOwner()->GetActorLocation();
				dir = FVector(dir.X, dir.Y, 0).GetSafeNormal();

				ASON_Projectile *proj = GetWorld()->SpawnActorDeferred<ASON_Projectile>(
					BaseParametrs.SpawnableClass,
					FTransform(dir.Rotation(), GetOwner()->GetActorLocation(), FVector(1.f)), 
					GetOwner(),
					nullptr,
					ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
				proj->SetParams(BaseParametrs.Damage * (1 + BonusParametrs.Damage));
				proj->FinishSpawning(FTransform(dir.Rotation(), GetOwner()->GetActorLocation(), FVector(1.f)));
			}
		}

		if (target == nullptr)
		{
			IsFiring = false;
			GetWorld()->GetTimerManager().ClearTimer(FireTimer);
		}
	}
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_BasicAbility.h"

#include "Kismet/KismetSystemLibrary.h"
#include "../SON_Projectile.h"
#include "../../Enemies/SON_Enemy.h"

USON_BasicAbility::USON_BasicAbility()
{
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.TickInterval = 0.1f;
	PrimaryComponentTick.bStartWithTickEnabled = false;

	ObjectChannels.Add(UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel4));
	TargetClasses.Add(ASON_Enemy::StaticClass());
}

void USON_BasicAbility::BeginPlay()
{
	Super::BeginPlay();
}

void USON_BasicAbility::DeactivateAbility()
{
	State = ESON_AbilityState::Ready;
	SetComponentTickEnabled(false);
}

void USON_BasicAbility::ActivateAbility()
{
	if (State != ESON_AbilityState::Ready)
		return;

	State = ESON_AbilityState::Active;

	//SetComponentTickEnabled(true);
	IsFiring = true;
	GetWorld()->GetTimerManager().SetTimer(
		FireTimer,
		this,
		&USON_BasicAbility::Fire,
		1.f / GetAbilityFireRate(),
		true,
		0.f);
}

void USON_BasicAbility::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (State == ESON_AbilityState::Active && !IsFiring && IsValid(GetOwner()))
	{
		TArray<AActor*> out;

		if (UKismetSystemLibrary::SphereOverlapActors(GetWorld(),
			GetOwner()->GetActorLocation(),
			GetAbilityRange(),
			ObjectChannels,
			NULL,
			TArray<AActor*>(),
			out))
		{
			for (auto& act : out)
				for (auto& cl : TargetClasses)
					if (act->IsA(cl))
						IsFiring = true;

			if (IsFiring)
				GetWorld()->GetTimerManager().SetTimer(
					FireTimer,
					this,
					&USON_BasicAbility::Fire,
					1.f / GetAbilityFireRate(),
					true,
					0.f);
		}  
	}
}

void USON_BasicAbility::Fire()
{
	if (State != ESON_AbilityState::Active)
	{
		IsFiring = false;
		GetWorld()->GetTimerManager().ClearTimer(FireTimer);
		return;
	}

	if (IsValid(GetOwner()))
	{
		/*TArray<AActor*> out;
		AActor* target = nullptr;
		float target_distance = 1e10;

		if (UKismetSystemLibrary::SphereOverlapActors(GetWorld(),
			GetOwner()->GetActorLocation(),
			GetAbilityRange(),
			ObjectChannels,
			NULL,
			TArray<AActor*>(),
			out))
		{
			TArray<AActor*> out_filtred;

			for (auto& act : out)
				for (auto& cl : TargetClasses)
					if (act->IsA(cl))
						out_filtred.AddUnique(act);

			for (auto& i : out_filtred)
			{
				float dist = GetOwner()->GetDistanceTo(i);
				if (target == nullptr || dist < target_distance)
				{
					target = i;
					target_distance = dist;
				}
			}

			if (IsValid(target))
			{
				FActorSpawnParameters asp;
				asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				asp.Owner = GetOwner();

				FVector dir = target->GetActorLocation() - GetOwner()->GetActorLocation();
				dir = FVector(dir.X, dir.Y, 0).GetSafeNormal();

				ASON_Projectile* proj = GetWorld()->SpawnActorDeferred<ASON_Projectile>(
					BaseParametrs.SpawnableClass,
					FTransform(dir.Rotation(), GetOwner()->GetActorLocation(), FVector(1.f)),
					GetOwner(),
					nullptr,
					ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
				proj->SetParams(GetAbilityDamage());
				proj->FinishSpawning(FTransform(dir.Rotation(), GetOwner()->GetActorLocation(), FVector(1.f)));
			}
		}

		if (target == nullptr)
		{
			IsFiring = false;
			GetWorld()->GetTimerManager().ClearTimer(FireTimer);
		} */

		FActorSpawnParameters asp;
		asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		asp.Owner = GetOwner();

		FVector dir = GetOwner()->GetActorForwardVector();
		dir = FVector(dir.X, dir.Y, 0).GetSafeNormal();

		ASON_Projectile* proj = GetWorld()->SpawnActorDeferred<ASON_Projectile>(
			BaseParametrs.SpawnableClass,
			FTransform(dir.Rotation(), GetOwner()->GetActorLocation(), FVector(1.f)),
			GetOwner(),
			nullptr,
			ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
		proj->SetParams(GetAbilityDamage());
		proj->FinishSpawning(FTransform(dir.Rotation(), GetOwner()->GetActorLocation(), FVector(1.f)));

	}
}

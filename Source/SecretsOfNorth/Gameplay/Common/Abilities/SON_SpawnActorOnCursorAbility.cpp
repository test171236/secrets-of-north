// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_SpawnActorOnCursorAbility.h"
#include "../../Character/SON_PlayerController.h"
#include "../../Character/SON_PlayerCharacter.h"
#include "SON_SpawnableActor.h"

USON_SpawnActorOnCursorAbility::USON_SpawnActorOnCursorAbility()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void USON_SpawnActorOnCursorAbility::ActivateAbility()
{
	if (IsValid(Controller) && Controller->IsHitUnderCursor() && State == ESON_AbilityState::Ready)
	{
		State = ESON_AbilityState::Cooldown;
		ASON_SpawnableActor *actor = GetWorld()->SpawnActorDeferred<ASON_SpawnableActor>(BaseParametrs.SpawnableClass,
			FTransform(Controller->HitResultUnderCursor().ImpactPoint),
			GetOwner());

		actor->InitParametrs(BaseParametrs, BonusParametrs);

		Cast<AActor>(actor)->FinishSpawning(FTransform(Controller->HitResultUnderCursor().ImpactPoint));

		GetWorld()->GetTimerManager().SetTimer(
			CooldownTimer,
			this,
			&USON_SpawnActorOnCursorAbility::CooldownEnd,
			BaseParametrs.CooldownTime / (1 + BonusParametrs.CooldownTime));
	}
}

void USON_SpawnActorOnCursorAbility::BeginPlay()
{
	Super::BeginPlay();

	ASON_PlayerCharacter* player = Cast<ASON_PlayerCharacter>(GetOwner());
	if (IsValid(player))
	{
		Controller = Cast<ASON_PlayerController>(player->GetController());
		State = ESON_AbilityState::Ready;
	}
}

void USON_SpawnActorOnCursorAbility::CooldownEnd()
{
	State = ESON_AbilityState::Ready;
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_PlayerAbility.h"

void USON_PlayerAbility::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void USON_PlayerAbility::SetupAbility(FName newName, const FSON_AbilityBaseParameters& newBaseParametrs, const FSON_AbilityBonusParameters& newBonusParametrs)
{
    Name = newName;
    BaseParametrs = newBaseParametrs;
    BonusParametrs = newBonusParametrs;
    State = ESON_AbilityState::Ready;
}

void USON_PlayerAbility::ApplyBonus(const FSON_AbilityBonusParameters& AbilityBonuses)
{
    BonusParametrs += AbilityBonuses;
}

float USON_PlayerAbility::GetAbilityDamage() const
{
    return BaseParametrs.Damage * (1 + BonusParametrs.Damage);
}

float USON_PlayerAbility::GetActiveTimeRemain() const
{
    UWorld* w = GetWorld();
    if (IsValid(w))
        return w->GetTimerManager().GetTimerRemaining(ActivationTimer);
    return 0.0f;
}

float USON_PlayerAbility::GetCooldownTimeRemain() const
{
    UWorld* w = GetWorld();
    if (IsValid(w))
        return w->GetTimerManager().GetTimerRemaining(CooldownTimer);
    return 0.0f;
}

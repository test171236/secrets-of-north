// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SON_SpawnableActor.h"
#include "SON_Beam.generated.h"

/**
 * 
 */
UCLASS()
class SECRETSOFNORTH_API ASON_Beam : public ASON_SpawnableActor
{
	GENERATED_BODY()
	
	UPROPERTY()
	AActor* ActorBegin; 
	UPROPERTY()
	AActor* ActorEnd;
	UPROPERTY(EditDefaultsOnly)
	class UNiagaraComponent* VFX;

	void BeginPlay() override;
	void Tick(float DeltaTime) override;

public:
	ASON_Beam();

	void SetupActors(AActor* newActorBegin, AActor* newActorEnd);
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_SpawnableActor.h"

// Sets default values
ASON_SpawnableActor::ASON_SpawnableActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ASON_SpawnableActor::InitParametrs(const FSON_AbilityBaseParameters& newBaseParametrs, const FSON_AbilityBonusParameters& newBonusParametrs)
{
	BaseParametrs = newBaseParametrs;
	BonusParametrs = newBonusParametrs;
}


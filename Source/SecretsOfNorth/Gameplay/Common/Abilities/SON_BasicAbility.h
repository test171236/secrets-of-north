// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SON_PlayerAbility.h"
#include "SON_BasicAbility.generated.h"

UCLASS()
class SECRETSOFNORTH_API USON_BasicAbility : public USON_PlayerAbility
{
	GENERATED_BODY()
	
	UPROPERTY()
	TArray<TSubclassOf<AActor>> TargetClasses;
	UPROPERTY()
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectChannels;

	FTimerHandle FireTimer;

	bool IsFiring = false;

	void Fire();

public:
	USON_BasicAbility();

protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void ActivateAbility() override;
	void DeactivateAbility() override;

};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SON_PlayerAbility.h"
#include "SON_TeleportAbility.generated.h"

UCLASS()
class SECRETSOFNORTH_API USON_TeleportAbility : public USON_PlayerAbility
{
	GENERATED_BODY()
	
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectChannels;

public:
	USON_TeleportAbility();
	virtual void ActivateAbility() override;

protected:
	virtual void BeginPlay() override;
	void CooldownEnd();

};

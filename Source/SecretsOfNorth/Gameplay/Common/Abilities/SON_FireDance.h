// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SON_PlayerAbility.h"
#include "SON_FireDance.generated.h"

/**
 * 
 */
UCLASS()
class SECRETSOFNORTH_API USON_FireDance : public USON_PlayerAbility
{
	GENERATED_BODY()
	
protected:
	int FireballNum = 3;
	float Radius = 300;
	float SmallRadius = 150;
	float RotSpeed = 180;
	float SmallRotSpeed = 400;

	UPROPERTY()
	TArray<class ASON_SpawnableActor *> FireSphere;

	void CooldownEnd();

public:
	USON_FireDance();

	virtual void ActivateAbility() override;
	void DeactivateAbility() override;
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_WindAbility_Many.h"

#include "SON_SpawnableActor.h"

void USON_WindAbility_Many::Fire()
{
	if (State != ESON_AbilityState::Active)
	{
		IsFiring = false;
		GetWorld()->GetTimerManager().ClearTimer(FireTimer);
		return;
	}

	if (IsValid(GetOwner()))
	{
		FActorSpawnParameters asp;
		asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		asp.Owner = GetOwner();

		const int NUM = 3;
		const float ANGLE = 15;
		for (int i = 0; i < NUM; i++)
		{
			FVector dir = GetOwner()->GetActorForwardVector().RotateAngleAxis(ANGLE * (i - (NUM - 1.f) / 2.f), FVector(0, 0, 1));
			dir = FVector(dir.X, dir.Y, 0).GetSafeNormal();

			ASON_SpawnableActor* sa = GetWorld()->SpawnActorDeferred<ASON_SpawnableActor>(
				BaseParametrs.SpawnableClass,
				FTransform(dir.Rotation(), GetOwner()->GetActorLocation(), FVector(1.f)),
				GetOwner(),
				nullptr,
				ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
			sa->InitParametrs(BaseParametrs, BonusParametrs);
			sa->FinishSpawning(FTransform(dir.Rotation(), GetOwner()->GetActorLocation(), FVector(1.f)));
		}
	}
}

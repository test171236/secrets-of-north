// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_WindAbility.h"

#include "Kismet/KismetSystemLibrary.h"
#include "SON_SpawnableActor.h"
#include "../../Enemies/SON_Enemy.h"

USON_WindAbility::USON_WindAbility()
{
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.TickInterval = 0.1f;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}

void USON_WindAbility::BeginPlay()
{
	Super::BeginPlay();
}

void USON_WindAbility::DeactivateAbility()
{
	State = ESON_AbilityState::Cooldown;
	SetComponentTickEnabled(false);

	GetWorld()->GetTimerManager().SetTimer(
		CooldownTimer,
		this,
		&USON_WindAbility::CooldownEnd,
		GetAbilityCooldownTime());
}

void USON_WindAbility::CooldownEnd()
{
	State = ESON_AbilityState::Ready;
}

void USON_WindAbility::ActivateAbility()
{
	if (State != ESON_AbilityState::Ready)
		return;

	State = ESON_AbilityState::Active;

	//SetComponentTickEnabled(true);
	IsFiring = true;
	GetWorld()->GetTimerManager().SetTimer(
		FireTimer,
		this,
		&USON_WindAbility::Fire,
		1.f / GetAbilityFireRate(),
		true,
		0.f);
	GetWorld()->GetTimerManager().SetTimer(
		ActivationTimer,
		this,
		&USON_WindAbility::DeactivateAbility,
		GetAbilityDurationTime());

}

void USON_WindAbility::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void USON_WindAbility::Fire()
{
	if (State != ESON_AbilityState::Active)
	{
		IsFiring = false;
		GetWorld()->GetTimerManager().ClearTimer(FireTimer);
		return;
	}

	if (IsValid(GetOwner()))
	{
		FActorSpawnParameters asp;
		asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		asp.Owner = GetOwner();

		FVector dir = GetOwner()->GetActorForwardVector();
		dir = FVector(dir.X, dir.Y, 0).GetSafeNormal();

		ASON_SpawnableActor* sa = GetWorld()->SpawnActorDeferred<ASON_SpawnableActor>(
			BaseParametrs.SpawnableClass,
			FTransform(dir.Rotation(), GetOwner()->GetActorLocation(), FVector(1.f)),
			GetOwner(),
			nullptr,
			ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
		sa->InitParametrs(BaseParametrs, BonusParametrs);
		sa->FinishSpawning(FTransform(dir.Rotation(), GetOwner()->GetActorLocation(), FVector(1.f)));
	}
}

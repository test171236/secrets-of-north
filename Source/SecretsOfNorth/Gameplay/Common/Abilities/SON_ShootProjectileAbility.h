// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SON_PlayerAbility.h"
#include "SON_ShootProjectileAbility.generated.h"

UCLASS( ClassGroup=(Ability), meta=(BlueprintSpawnableComponent) )
class SECRETSOFNORTH_API USON_ShootProjectileAbility : public USON_PlayerAbility
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<AActor>> TargetClasses;
	UPROPERTY(EditAnywhere)
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectChannels;

	FTimerHandle FireTimer;
	
	bool IsFiring = false;

	void Fire();

public:	
	USON_ShootProjectileAbility();

	bool IsAlwaysActive = false;

protected:
	virtual void BeginPlay() override;

	void CooldownEnd();

public:	
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void ActivateAbility() override;
	void DeactivateAbility() override;
};

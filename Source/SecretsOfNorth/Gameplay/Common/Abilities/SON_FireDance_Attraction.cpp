// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_FireDance_Attraction.h"

#include "SON_SpawnableActor.h"
#include "Kismet/KismetSystemLibrary.h"
#include "../../Enemies/SON_Enemy.h"

void USON_FireDance_Attraction::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	TArray<TSubclassOf<AActor>> TargetClasses;
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectChannels;

	ObjectChannels.Add(UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel4));
	TargetClasses.Add(ASON_Enemy::StaticClass());

	for (int i = 0; i < FireSphere.Num(); i++)
		if (IsValid(FireSphere[i]))
		{
			float time = GetWorld()->GetTimeSeconds();
			FVector loc = GetOwner()->GetActorLocation() + FRotator(0, i * 360.f / FireballNum + time * RotSpeed, 0).RotateVector(FVector(Radius, 0, 0) +
				FRotator(0, i * 360.f / FireballNum + time * SmallRotSpeed, 0).RotateVector(FVector(SmallRadius, 0, 0)));

			TArray<AActor*> out;
			AActor* target = nullptr;
			float target_distance = 1e10;

			if (UKismetSystemLibrary::SphereOverlapActors(GetWorld(),
				FireSphere[i]->GetActorLocation(),
				AttractionRadius,
				ObjectChannels,
				NULL,
				TArray<AActor*>(),
				out))
			{
				TArray<ASON_Enemy*> out_filtred;

				for (auto& act : out)
					for (auto& cl : TargetClasses)
						if (act->IsA(cl))
							out_filtred.AddUnique(Cast<ASON_Enemy>(act));

				for (auto& of : out_filtred)
				{
					float dist = FireSphere[i]->GetDistanceTo(of);
					if ((target == nullptr || dist < target_distance) && !of->IsDead())
					{
						target = of;
						target_distance = dist;
					}
				}

				if (IsValid(target))
				{
					loc = target->GetActorLocation();
				}
			}

			float dist = (FireSphere[i]->GetActorLocation() - loc).Length();
			float delta_len = Speed * DeltaTime;
			FVector dir = loc - FireSphere[i]->GetActorLocation();
			dir.Normalize();

			FireSphere[i]->AddActorWorldOffset(dir * FMath::Min(delta_len, dist));

		}
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SON_PlayerAbility.h"
#include "SON_WindAbility.generated.h"

/**
 * 
 */
UCLASS()
class SECRETSOFNORTH_API USON_WindAbility : public USON_PlayerAbility
{
	GENERATED_BODY()

public:
	USON_WindAbility();

protected:
	virtual void BeginPlay() override;

	FTimerHandle FireTimer;

	bool IsFiring = false;

	virtual void Fire();
	void CooldownEnd();

public:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void ActivateAbility() override;
	void DeactivateAbility() override;

};

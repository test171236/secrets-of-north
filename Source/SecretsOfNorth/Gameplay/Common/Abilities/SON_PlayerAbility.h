// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SON_PlayerAbility.generated.h"

USTRUCT(BlueprintType)
struct FSON_AbilityBaseParameters
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Damage = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DurationTime = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CooldownTime = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Range = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float FireRate = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AOERadius = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Effect = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class ASON_SpawnableActor> SpawnableClass;
};

USTRUCT(BlueprintType)
struct FSON_AbilityBonusParameters
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Damage = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DurationTime = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float CooldownTime = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Range = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float FireRate = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AOERadius = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Effect = 0;

	FSON_AbilityBonusParameters& operator+=(const FSON_AbilityBonusParameters& abp)
	{
		Damage			+= abp.Damage;
		DurationTime	+= abp.DurationTime;
		CooldownTime	+= abp.CooldownTime;
		Range			+= abp.Range;
		FireRate		+= abp.FireRate;
		AOERadius		+= abp.AOERadius;
		Effect			+= abp.Effect;

		return *this;
	}
};

UENUM(BlueprintType)
enum class ESON_AbilityState : uint8 {
	Ready				UMETA(DisplayName = "Ready"),
	Active				UMETA(DisplayName = "Active"),
	Cooldown			UMETA(DisplayName = "Cooldown"),
};

UCLASS()
class USON_PlayerAbility : public UActorComponent
{
	GENERATED_BODY()
protected:
	FName Name;
	ESON_AbilityState State;
	FSON_AbilityBaseParameters BaseParametrs;
	FTimerHandle ActivationTimer;
	FTimerHandle CooldownTimer;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UPROPERTY(BlueprintReadOnly)
	FSON_AbilityBonusParameters BonusParametrs;

	virtual void ActivateAbility() {}
	virtual void DeactivateAbility() {}
	void SetupAbility(FName newName, const FSON_AbilityBaseParameters& newBaseParametrs, const FSON_AbilityBonusParameters& newBonusParametrs);
	void ApplyBonus(const FSON_AbilityBonusParameters& AbilityBonuses);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	ESON_AbilityState GetState() const { return State; }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const FSON_AbilityBaseParameters& GetBaseParametrs() const { return BaseParametrs; }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetAbilityDamage() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetAbilityDurationTime() const { return BaseParametrs.DurationTime * (1 + BonusParametrs.DurationTime); }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetAbilityCooldownTime() const { return BaseParametrs.CooldownTime / (1 + BonusParametrs.CooldownTime); }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetAbilityRange() const { return BaseParametrs.Range * (1 + BonusParametrs.Range); }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetAbilityFireRate() const { return BaseParametrs.FireRate * (1 + BonusParametrs.FireRate); }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetAbilityAOERadius() const { return BaseParametrs.AOERadius * (1 + BonusParametrs.AOERadius); }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetAbilityEffect() const { return BaseParametrs.Effect * (1 + BonusParametrs.Effect); }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetActiveTimeRemain() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCooldownTimeRemain() const;
};

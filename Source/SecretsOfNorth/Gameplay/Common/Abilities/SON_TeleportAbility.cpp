// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_TeleportAbility.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/CapsuleComponent.h"
#include "../../Character/SON_PlayerCharacter.h"
#include "../../Character/SON_PlayerController.h"
#include "../../Enemies/SON_Enemy.h"

USON_TeleportAbility::USON_TeleportAbility()
{
	PrimaryComponentTick.bCanEverTick = false;

	ObjectChannels.Add(UEngineTypes::ConvertToObjectType(ECC_WorldStatic));
	ObjectChannels.Add(UEngineTypes::ConvertToObjectType(ECC_WorldDynamic));
	ObjectChannels.Add(UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel5));
}

void USON_TeleportAbility::ActivateAbility()
{
	ASON_PlayerCharacter* player = Cast<ASON_PlayerCharacter>(GetOwner());

	if (IsValid(player))
	{
		ASON_PlayerController* controller = Cast<ASON_PlayerController>(player->GetController());
		if (IsValid(controller) && controller->IsHitUnderCursor())
		{
			FVector loc = controller->HitResultUnderCursor().ImpactPoint;
			loc.Z = player->GetActorLocation().Z;

			FHitResult out_hit;

			UKismetSystemLibrary::CapsuleTraceSingleForObjects(
				GetWorld(),
				player->GetActorLocation(),
				loc,
				player->GetCapsuleComponent()->GetScaledCapsuleRadius(),
				player->GetCapsuleComponent()->GetScaledCapsuleHalfHeight(),
				ObjectChannels,
				false,
				TArray<AActor*>({GetOwner()}),
				EDrawDebugTrace::None,
				out_hit,
				true);
			
			if (out_hit.bBlockingHit)
				loc = out_hit.Location;

			TArray<FHitResult> enemy_hits;

			UKismetSystemLibrary::CapsuleTraceMultiForObjects(
				GetWorld(),
				player->GetActorLocation(),
				loc,
				player->GetCapsuleComponent()->GetScaledCapsuleRadius(),
				player->GetCapsuleComponent()->GetScaledCapsuleHalfHeight(),
				TArray<TEnumAsByte<EObjectTypeQuery>>({ UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel4) }),
				false,
				TArray<AActor*>({ GetOwner() }),
				EDrawDebugTrace::None,
				enemy_hits,
				true);

			for (auto& hr : enemy_hits)
			{
				ASON_Enemy* enemy = Cast<ASON_Enemy>(hr.GetActor());
				if (IsValid(enemy) && hr.bBlockingHit)
				{
					FVector launch_vec = -hr.ImpactNormal * 200;
					enemy->LaunchCharacter(FVector(launch_vec.X, launch_vec.Y, 600), true, true);
				}
			}

			Cast<AActor>(player)->SetActorLocation(loc);

			GetWorld()->GetTimerManager().SetTimer(
				CooldownTimer,
				this,
				&USON_TeleportAbility::CooldownEnd,
				BaseParametrs.CooldownTime / (1 + BonusParametrs.CooldownTime));
		}
	}
}

void USON_TeleportAbility::BeginPlay()
{
	Super::BeginPlay();

	State = ESON_AbilityState::Ready;
}

void USON_TeleportAbility::CooldownEnd()
{
	State = ESON_AbilityState::Ready;
}

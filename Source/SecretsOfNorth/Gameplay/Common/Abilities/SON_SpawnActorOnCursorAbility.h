// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SON_PlayerAbility.h"
#include "SON_SpawnActorOnCursorAbility.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SECRETSOFNORTH_API USON_SpawnActorOnCursorAbility : public USON_PlayerAbility
{
	GENERATED_BODY()

	UPROPERTY()
	class ASON_PlayerController* Controller;

public:	
	USON_SpawnActorOnCursorAbility();
	virtual void ActivateAbility() override;

protected:
	virtual void BeginPlay() override;
	void CooldownEnd();
};

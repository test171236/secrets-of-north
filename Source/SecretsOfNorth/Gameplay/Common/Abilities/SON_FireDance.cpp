// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_FireDance.h"
#include "SON_SpawnableActor.h"
#include "../../Character/SON_PlayerCharacter.h"

USON_FireDance::USON_FireDance()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}

void USON_FireDance::DeactivateAbility()
{
	for (auto& actor : FireSphere)
		actor->Destroy();
	FireSphere.Empty();

	State = ESON_AbilityState::Cooldown;
	SetComponentTickEnabled(false);
	GetWorld()->GetTimerManager().SetTimer(
		CooldownTimer,
		this,
		&USON_FireDance::CooldownEnd,
		GetAbilityCooldownTime());

	/*ASON_PlayerCharacter* player = Cast<ASON_PlayerCharacter>(GetOwner());
	if (IsValid(player))
		player->StopAbilitySound(Name);*/
}

void USON_FireDance::CooldownEnd()
{
	State = ESON_AbilityState::Ready;
}

void USON_FireDance::ActivateAbility()
{
	if (State != ESON_AbilityState::Ready)
		return;

	int ID = -1;

	if (FireSphere.Num() < FireballNum)
		ID = FireSphere.Num();
	else for (int i = 0; i < FireSphere.Num(); i++)
		if (!IsValid(FireSphere[i]))
		{
			ID = i;
			break;
		}

	if (ID < 0)
		return;

	FTransform trans = FTransform(GetOwner()->GetActorLocation() + FRotator(0, ID * 360.f / FireballNum, 0).RotateVector(FVector(Radius, 0, 0)));
	ASON_SpawnableActor* actor = GetWorld()->SpawnActorDeferred<ASON_SpawnableActor>(
		BaseParametrs.SpawnableClass,
		trans,
		GetOwner());

	actor->InitParametrs(BaseParametrs, BonusParametrs);
	Cast<AActor>(actor)->FinishSpawning(trans);
	
	if (ID >= FireSphere.Num())
		FireSphere.Add(actor);
	else
		FireSphere[ID] = actor;

	State = ESON_AbilityState::Cooldown;

	GetWorld()->GetTimerManager().SetTimer(
		ActivationTimer,
		this,
		&USON_FireDance::CooldownEnd,
		GetAbilityCooldownTime());

	SetComponentTickEnabled(true);

	/*ASON_PlayerCharacter* player = Cast<ASON_PlayerCharacter>(GetOwner());
	if (IsValid(player))
		player->PlayAbilitySound(Name);	*/
}

void USON_FireDance::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	for (int i = 0; i < FireSphere.Num(); i++)
		if (IsValid(FireSphere[i]))
		{
			float time = GetWorld()->GetTimeSeconds();
			FVector loc = GetOwner()->GetActorLocation() + FRotator(0, i * 360.f / FireballNum + time * RotSpeed, 0).RotateVector(FVector(Radius, 0, 0) + 
				FRotator(0, i * 360.f / FireballNum + time * SmallRotSpeed, 0).RotateVector(FVector(SmallRadius, 0, 0)));
			FireSphere[i]->SetActorLocation(loc);
		} 
}
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SON_WindAbility.h"
#include "SON_WindAbility_Many.generated.h"

/**
 * 
 */
UCLASS()
class SECRETSOFNORTH_API USON_WindAbility_Many : public USON_WindAbility
{
	GENERATED_BODY()

protected:
	virtual void Fire() override;
};

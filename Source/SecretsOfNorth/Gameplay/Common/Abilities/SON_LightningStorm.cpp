// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_LightningStorm.h"
#include "Kismet/GameplayStatics.h"
#include "SON_SpawnableActor.h"
#include "../../Character/SON_PlayerCharacter.h"

void USON_LightningStorm::DeactivateAbility()
{
	State = ESON_AbilityState::Cooldown;
	SetComponentTickEnabled(false);
	GetWorld()->GetTimerManager().ClearTimer(FireTimer);
	GetWorld()->GetTimerManager().SetTimer(
		CooldownTimer,
		this,
		&USON_LightningStorm::CooldownEnd,
		GetAbilityCooldownTime());

	/*ASON_PlayerCharacter* player = Cast<ASON_PlayerCharacter>(GetOwner());
	if (IsValid(player))
		player->StopAbilitySound(Name);	*/
}

void USON_LightningStorm::CooldownEnd()
{
	State = ESON_AbilityState::Ready;
}

void USON_LightningStorm::Fire()
{
	const int k = 10;
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectChannels;

	ObjectChannels.Add(UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel3));
	ObjectChannels.Add(UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel5));

	for (int i = 0; i < k; i++)
	{
		FHitResult hr;
		FVector loc = FVector(GetAbilityRange() * (2 * FMath::FRand() - 1), GetAbilityRange() * (2 * FMath::FRand() - 1), 0);
		while (loc.Length() > GetAbilityRange())
			loc = FVector(GetAbilityRange() * (2 * FMath::FRand() - 1), GetAbilityRange() * (2 * FMath::FRand() - 1), 0);
		loc += GetOwner()->GetActorLocation();

		UKismetSystemLibrary::LineTraceSingle(
			GetWorld(),
			loc + FVector(0, 0, 10000),
			loc + FVector(0, 0, -1) * 1e6,
			UEngineTypes::ConvertToTraceType(ECC_Visibility),
			false,
			TArray<AActor*>(),
			EDrawDebugTrace::None,
			hr,
			false);

		if (hr.bBlockingHit)
		{
			TArray<AActor*> out;

			/*if (!UKismetSystemLibrary::SphereOverlapActors(GetWorld(),
				loc,
				GetAbilityAOERadius(),
				ObjectChannels,
				NULL,
				TArray<AActor*>(),
				out))*/
			{ 


				ASON_SpawnableActor* actor = GetWorld()->SpawnActorDeferred<ASON_SpawnableActor>(BaseParametrs.SpawnableClass,
					FTransform(hr.ImpactPoint),
					GetOwner());

				actor->InitParametrs(BaseParametrs, BonusParametrs);

				Cast<AActor>(actor)->FinishSpawning(FTransform(hr.ImpactPoint));
				break;
			}
		}
	}
}

void USON_LightningStorm::ActivateAbility()
{
	if (State != ESON_AbilityState::Ready)
		return;

	State = ESON_AbilityState::Active;

	GetWorld()->GetTimerManager().SetTimer(
		ActivationTimer,
		this,
		&USON_LightningStorm::DeactivateAbility,
		GetAbilityDurationTime());

	GetWorld()->GetTimerManager().SetTimer(
		FireTimer,
		this,
		&USON_LightningStorm::Fire,
		1.f / GetAbilityFireRate(),
		true,
		0.f);

	/*ASON_PlayerCharacter* player = Cast<ASON_PlayerCharacter>(GetOwner());
	if (IsValid(player))
		player->PlayAbilitySound(Name);	*/
}

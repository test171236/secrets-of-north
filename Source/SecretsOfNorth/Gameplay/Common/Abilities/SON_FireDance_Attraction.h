// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SON_FireDance.h"
#include "SON_FireDance_Attraction.generated.h"

/**
 * 
 */
UCLASS()
class SECRETSOFNORTH_API USON_FireDance_Attraction : public USON_FireDance
{
	GENERATED_BODY()
	
	float AttractionRadius = 200;
	float Speed = 1000;

public:
	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction);

};

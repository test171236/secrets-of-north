// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SON_PlayerAbility.h"
#include "SON_SpawnableActor.generated.h"

UCLASS()
class SECRETSOFNORTH_API ASON_SpawnableActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ASON_SpawnableActor();

protected:
	UPROPERTY(BlueprintReadOnly, Meta = (ExposeOnSpawn = true))
	FSON_AbilityBaseParameters BaseParametrs;
	UPROPERTY(BlueprintReadOnly, Meta = (ExposeOnSpawn = true))
	FSON_AbilityBonusParameters BonusParametrs;

public:	

	void InitParametrs(const FSON_AbilityBaseParameters& newBaseParametrs, const FSON_AbilityBonusParameters& newBonusParametrs);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetActorDamage() const { return BaseParametrs.Damage * (1 + BonusParametrs.Damage); }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetActorDurationTime() const { return BaseParametrs.DurationTime * (1 + BonusParametrs.DurationTime); }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetActorCooldownTime() const { return BaseParametrs.CooldownTime / (1 + BonusParametrs.CooldownTime); }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetActorRange() const { return BaseParametrs.Range * (1 + BonusParametrs.Range); }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetActorFireRate() const { return BaseParametrs.FireRate * (1 + BonusParametrs.FireRate); }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetActorAOERadius() const { return BaseParametrs.AOERadius * (1 + BonusParametrs.AOERadius); }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetActorEffect() const { return BaseParametrs.Effect * (1 + BonusParametrs.Effect); }

};

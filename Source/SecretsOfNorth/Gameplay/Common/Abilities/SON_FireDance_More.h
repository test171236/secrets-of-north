// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SON_FireDance.h"
#include "SON_FireDance_More.generated.h"

/**
 * 
 */
UCLASS()
class SECRETSOFNORTH_API USON_FireDance_More : public USON_FireDance
{
	GENERATED_BODY()
	
public:
	USON_FireDance_More();

};

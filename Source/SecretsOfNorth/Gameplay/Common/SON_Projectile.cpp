// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_Projectile.h"

#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"

ASON_Projectile::ASON_Projectile()
{
	PrimaryActorTick.bCanEverTick = false;

	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->SetSphereRadius(5.f);
	CollisionComp->BodyInstance.SetCollisionProfileName("CharacterProjectile");
	CollisionComp->OnComponentBeginOverlap.AddDynamic(this, &ASON_Projectile::OnBeginOverlap);		// set up a notification for when this component hits something blocking
	CollisionComp->OnComponentHit.AddDynamic(this, &ASON_Projectile::OnHit);		// set up a notification for when this component hits something blocking
	
	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;
	CollisionComp->SetCanEverAffectNavigation(false);

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = false;
	ProjectileMovement->bShouldBounce = true;
	ProjectileMovement->bInitialVelocityInLocalSpace = false;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

	DamageInfo.DamageTypeClass = USON_ProjectileDamage::StaticClass();
}

void ASON_Projectile::BeginPlay()
{
	Super::BeginPlay();
}

void ASON_Projectile::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	//SetActorRotation(FRotator(0, 0, 90));
	//AddActorWorldRotation(Transform.Rotator());
	ProjectileMovement->Velocity = ProjectileMovement->InitialSpeed * GetActorForwardVector();
}

void ASON_Projectile::SetParams(float newDamage)
{
	Damage = newDamage;
}

void ASON_Projectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		OtherActor->TakeDamage(Damage, DamageInfo, nullptr, this);
	}
	Destroy();
}

void ASON_Projectile::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		OtherActor->TakeDamage(Damage, DamageInfo, nullptr, this);
	}
}


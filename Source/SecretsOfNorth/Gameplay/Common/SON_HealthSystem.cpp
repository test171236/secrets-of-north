// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_HealthSystem.h"

USON_HealthSystem::USON_HealthSystem()
{
	PrimaryComponentTick.bCanEverTick = false;
}


void USON_HealthSystem::BeginPlay()
{
	Super::BeginPlay();
	
}

void USON_HealthSystem::SetMaxHP(float newMaxHP)
{
	MaxHP = newMaxHP;
	CurHP = newMaxHP;
	isDead = false;
}

void USON_HealthSystem::ChangeMaxHP(float newMaxHP)
{
	MaxHP = newMaxHP;
	if (CurHP > MaxHP)
		CurHP = MaxHP;
}

void USON_HealthSystem::SetCurrentHP(float newcurHP)
{
	CurHP = newcurHP;
	if (newcurHP > 0)
		isDead = false;
	else
		isDead = true;
}

float USON_HealthSystem::ReduceHP(float deltaHP)
{
	if (isDead || deltaHP <= 0.f)
		return 0.0f;

	float res = 0;

	if (CurHP > deltaHP)
	{
		res = deltaHP;
		CurHP -= deltaHP;
	}
	else
	{
		isDead = true;
		res = CurHP;
		CurHP = 0.f;
		OnDied.Broadcast();
	}

	return res;
}

float USON_HealthSystem::AddHP(float deltaHP)
{
	if (isDead || deltaHP <= 0.f)
		return 0.0f;

	float res;
	if (CurHP + deltaHP >= MaxHP)
	{
		res = MaxHP - CurHP;
		CurHP = MaxHP;
		return res;
	}
	else
	{
		res = deltaHP;
		CurHP += deltaHP;
		return res;
	}
}

void USON_HealthSystem::RestoreFullHP()
{
	isDead = false;
	CurHP = MaxHP;
}

float USON_HealthSystem::GetCurHP()	const
{
	return CurHP;
}

float USON_HealthSystem::GetMaxHP()	const
{
	return MaxHP;
}

bool USON_HealthSystem::GetIsDead() const
{
	return isDead;
}

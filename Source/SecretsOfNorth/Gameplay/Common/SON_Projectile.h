// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SON_DamageTypes.h"
#include "Abilities/SON_SpawnableActor.h"
#include "SON_Projectile.generated.h"

UCLASS()
class SECRETSOFNORTH_API ASON_Projectile : public ASON_SpawnableActor
{
	GENERATED_BODY()

public:	
	ASON_Projectile();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FSON_DamageInfo DamageInfo;

public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USphereComponent* CollisionComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UProjectileMovementComponent* ProjectileMovement;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	class USphereComponent* GetCollisionComp() const { return CollisionComp; }
	class UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }

	virtual void OnConstruction(const FTransform& Transform) override;

	void SetParams(float newDamage);
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_Tower.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "../Common/SON_HealthSystem.h"

ASON_Tower::ASON_Tower()
{
	PrimaryActorTick.bCanEverTick = false;

	Capsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	Capsule->SetCapsuleSize(42.f, 96.0f);
	Capsule->SetCollisionProfileName(FName("Tower"));
	SetRootComponent(Capsule);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetCollisionProfileName(FName("NoCollision"));
	StaticMesh->SetupAttachment(Capsule);

	HealthSystem = CreateDefaultSubobject<USON_HealthSystem>(TEXT("HealthSystem"));
	HealthSystem->SetMaxHP(5);
}

void ASON_Tower::BeginPlay()
{
	Super::BeginPlay();

	if (UseMeshCollision)
	{
		Capsule->SetCollisionProfileName(FName("NoCollision"));
		StaticMesh->SetCollisionProfileName(FName("Tower"));
	}
}


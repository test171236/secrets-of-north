// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_MainTree.h"

#include "Components/PointLightComponent.h"
#include "Components/CapsuleComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "../../Game/SON_AutobattleGameMode.h"
#include "../Character/SON_PlayerCharacter.h"

ASON_MainTree::ASON_MainTree()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	Capsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	Capsule->SetCapsuleSize(42.f, 96.0f);
	Capsule->SetCollisionProfileName(FName("Tower"));
	Capsule->SetGenerateOverlapEvents(true);
	Capsule->bDynamicObstacle = true;
	Capsule->SetCanEverAffectNavigation(true);
	SetRootComponent(Capsule);

	HealthSystem = CreateDefaultSubobject<USON_HealthSystem>(TEXT("HealthSystem"));
	HealthSystem->SetMaxHP(MaxHP);
	HealthSystem->SetCurrentHP(StartHP);
	HealthSystem->OnDied.AddDynamic(this, &ASON_MainTree::Death);

	Light = CreateDefaultSubobject<UPointLightComponent>(TEXT("Light"));
	Light->SetupAttachment(RootComponent);
	Light->SetAttenuationRadius(0);
	Light->SetSourceRadius(0);
	Light->SetShadowBias(0.5);
	Light->bUseInverseSquaredFalloff = false;
	Light->LightFalloffExponent = 1e-9;

	NiagaraLeaves = CreateDefaultSubobject<UNiagaraComponent>(TEXT("NiagaraLeaves"));
}

void ASON_MainTree::OnValidRitm()
{
	ASON_AutobattleGameMode* gm = Cast<ASON_AutobattleGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(gm))
	{
		ASON_PlayerCharacter* player = gm->GetPlayerCharacter();
		if (IsValid(player))
		{
			float dist = GetHorizontalDistanceTo(player);
			if (dist <= CurrentLightRadius || dist <= GrowRadius)
				HealthSystem->AddHP(HPIncRitmDelta);
		}
	}
}

void ASON_MainTree::BeginPlay()
{
	Super::BeginPlay();
	
	ASON_AutobattleGameMode* gm = Cast<ASON_AutobattleGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(gm))
	{
		gm->SetupMainTree(this);
	}

	HealthSystem->SetMaxHP(MaxHP);
	HealthSystem->SetCurrentHP(StartHP);
	BaseRadius = Light->GetRelativeLocation().Z + Capsule->GetScaledCapsuleHalfHeight();
	CurrentLightRadius = MaxLightRadius * StartHP / MaxHP;
	Light->SetAttenuationRadius(sqrt(BaseRadius * BaseRadius + CurrentLightRadius * CurrentLightRadius));
	//Light->SetSourceRadius(sqrt(BaseRadius * BaseRadius + CurrentLightRadius * CurrentLightRadius) / 2);
	Light->SetSourceRadius(LightSourceRadius);
	NiagaraLeaves->SetFloatParameter(TEXT("User.Alpha"), StartHP / MaxHP);
}

void ASON_MainTree::Death()
{
}

float ASON_MainTree::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (isInvincible)
		return 0.f;

	float res = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	HealthSystem->ReduceHP(res);

	return res;
}

float GetXYDistance(FVector v1, FVector v2)
{
	return (FVector(v1.X, v1.Y, 0) - FVector(v2.X, v2.Y, 0)).Length();
}

void ASON_MainTree::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ASON_AutobattleGameMode* gm = Cast<ASON_AutobattleGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(gm))
	{
		ASON_PlayerCharacter* player = gm->GetPlayerCharacter();
		if (IsValid(player))
		{
			float dist = GetHorizontalDistanceTo(player);
			if (dist <= CurrentLightRadius || dist <= GrowRadius)
				HealthSystem->AddHP(HPIncSpeed * DeltaTime);
		}
	}

	float CurrentTargetLightRadius = MaxLightRadius * HealthSystem->GetCurHP() / HealthSystem->GetMaxHP();

	if (CurrentLightRadius < CurrentTargetLightRadius)
	{
		CurrentLightRadius += DeltaTime * LightRadiusExpandSpeed;
		if (CurrentLightRadius > CurrentTargetLightRadius)
			CurrentLightRadius = CurrentTargetLightRadius;
	}
	if (CurrentLightRadius > CurrentTargetLightRadius)
	{
		CurrentLightRadius -= DeltaTime * LightRadiusExpandSpeed;
		if (CurrentLightRadius < CurrentTargetLightRadius)
			CurrentLightRadius = CurrentTargetLightRadius;
	}

	if (CurrentLightRadius >= MaxLightRadius)
	{
		CurrentLightRadius = MaxLightRadius;
		isInvincible = true;
		SetActorTickEnabled(false);
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(),
			NiagaraTreeComplite,
			GetActorLocation());
		UGameplayStatics::PlaySoundAtLocation(GetWorld(),
			TreeCompliteSound,
			GetActorLocation());
		if (IsValid(gm))
			gm->MainTreeComplite();
	}

	Light->SetAttenuationRadius(sqrt(BaseRadius * BaseRadius + CurrentLightRadius * CurrentLightRadius));
	Light->SetSourceRadius(LightSourceRadius);
	NiagaraLeaves->SetFloatParameter(TEXT("User.Alpha"), CurrentLightRadius / MaxLightRadius);
}


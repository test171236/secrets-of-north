// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Common/SON_HealthSystem.h"
#include "SON_LesserTree.generated.h"

UCLASS()
class SECRETSOFNORTH_API ASON_LesserTree : public AActor
{
	GENERATED_BODY()
	
	float CurrentLightRadius = 0;
	float BaseRadius;
	float BaseIntensity;
	
	UPROPERTY()
	ASON_LesserTree* NextTree;
	bool isNextTreeSet = false;

public:	
	ASON_LesserTree();

	UFUNCTION(BlueprintCallable)
	float GetCurrentRadius() const { return CurrentLightRadius; }
	UFUNCTION(BlueprintCallable)
	float GetMaxRadius() const { return MaxLightRadius; }

	void SetNextTree(ASON_LesserTree* newNextTree) { NextTree = newNextTree; isNextTreeSet = true; }
	ASON_LesserTree* GetNextTree() const { return NextTree; }
	bool GetIsNextTreeSet() const { return isNextTreeSet; }
	bool GetIsDead() const { return HealthSystem->GetIsDead(); }

	UFUNCTION()
	void OnValidRitm();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UPointLightComponent* Light;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UCapsuleComponent* Capsule;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USON_HealthSystem* HealthSystem;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxLightRadius = 1000;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float StartHP = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxHP = 20;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float LightRadiusExpandSpeed = 500;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float LightIntensitySpeed = 0.5;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float GrowRadius = 300;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float HPIncSpeed = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float HPIncRitmDelta = 2;

	UFUNCTION()
	void Death();

	virtual void BeginPlay() override;
	
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

public:	
	virtual void Tick(float DeltaTime) override;

	void ActivateTree();
};

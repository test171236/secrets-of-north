// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_BuildingTower.h"
#include "Components/CapsuleComponent.h"
#include "Components/StaticMeshComponent.h"
#include "SON_Tower.h"

ASON_BuildingTower::ASON_BuildingTower()
{
	PrimaryActorTick.bCanEverTick = false;

	Capsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	Capsule->SetCapsuleSize(42.f, 96.0f);
	Capsule->SetCollisionProfileName(FName("TowerOverlap"));
	SetRootComponent(Capsule);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetCollisionProfileName(FName("NoCollision"));
	StaticMesh->SetupAttachment(RootComponent);

	OnActorBeginOverlap.AddDynamic(this, &ASON_BuildingTower::BeginOverlap);
	OnActorEndOverlap.AddDynamic(this, &ASON_BuildingTower::EndOverlap);
}

void ASON_BuildingTower::SetupTowerClass(const TSubclassOf<class ASON_Tower>& NewTowerClass)
{
	if (!IsValid(NewTowerClass))
		return;

	TowerClass = NewTowerClass;
	ASON_Tower* tower = Cast<ASON_Tower>(TowerClass->GetDefaultObject());

	if (IsValid(tower))
	{
		UseMeshCollision = tower->GetUseMeshCollision();
		Capsule->SetCapsuleSize(tower->GetCapsule()->GetScaledCapsuleRadius(),
			tower->GetCapsule()->GetScaledCapsuleHalfHeight_WithoutHemisphere());

		StaticMesh->SetStaticMesh(tower->GetStaticMesh());
		StaticMesh->SetMaterial(0, NoOverlapsMaterial);

		if (UseMeshCollision)
		{
			Capsule->SetCollisionProfileName(FName("NoCollision"));
			StaticMesh->SetCollisionProfileName(FName("TowerOverlap"));
		}
	}
}

void ASON_BuildingTower::BeginPlay()
{
	Super::BeginPlay();
	
	SetupTowerClass(TowerClass);
}

ASON_Tower* ASON_BuildingTower::Build(const FTransform& trans) const
{
	FActorSpawnParameters asp;
	asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	if (GetCanBePlaced())
		return GetWorld()->SpawnActor<ASON_Tower>(TowerClass, trans, asp);
	return nullptr;
}

void ASON_BuildingTower::BeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (IsValid(OtherActor))
		OverlapCount++;

	if (OverlapCount == 1)
		StaticMesh->SetMaterial(0, ExistOverlapsMaterial);
}

void ASON_BuildingTower::EndOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (IsValid(OtherActor))
		OverlapCount--;

	if (OverlapCount == 0)
		StaticMesh->SetMaterial(0, NoOverlapsMaterial);
}


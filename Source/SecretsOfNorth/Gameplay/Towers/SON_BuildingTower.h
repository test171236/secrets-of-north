// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SON_BuildingTower.generated.h"

UCLASS()
class SECRETSOFNORTH_API ASON_BuildingTower : public AActor
{
	GENERATED_BODY()
	
	UPROPERTY()
	class UStaticMeshComponent* StaticMesh;
	UPROPERTY()
	class UCapsuleComponent* Capsule;

public:
	UPROPERTY(BlueprintReadWrite, Meta = (ExposeOnSpawn = "true"))
	TSubclassOf<class ASON_Tower> TowerClass;
	UPROPERTY(BlueprintReadWrite, Meta = (ExposeOnSpawn = "true"))
	bool UseMeshCollision = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UMaterialInstance* NoOverlapsMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UMaterialInstance* ExistOverlapsMaterial;

	ASON_BuildingTower();
	const class UCapsuleComponent* GetCapsule() const { return Capsule; }

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetCanBePlaced() const { return OverlapCount == 0; }

	UFUNCTION(BlueprintCallable)
	void SetupTowerClass(const TSubclassOf<class ASON_Tower>& NewTowerClass);

	UFUNCTION(BlueprintCallable)
	class ASON_Tower* Build(const FTransform& trans) const;

protected:
	UPROPERTY(BlueprintReadOnly)
	int OverlapCount = 0;

	virtual void BeginPlay() override;

	UFUNCTION()
	void BeginOverlap(AActor* OverlappedActor, AActor* OtherActor);
	UFUNCTION()
	void EndOverlap(AActor* OverlappedActor, AActor* OtherActor);
};

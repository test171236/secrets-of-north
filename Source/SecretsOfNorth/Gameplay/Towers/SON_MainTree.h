// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Common/SON_HealthSystem.h"
#include "SON_MainTree.generated.h"

UCLASS()
class SECRETSOFNORTH_API ASON_MainTree : public AActor
{
	GENERATED_BODY()
	
	float CurrentLightRadius = 0;
	float BaseRadius;
	bool isInvincible = false;

public:	
	// Sets default values for this actor's properties
	ASON_MainTree();

	USON_HealthSystem::FOnDied& GetOnDiedDelegate() { return HealthSystem->OnDied; }

	UFUNCTION(BlueprintCallable)
	float GetCurrentRadius() const { return CurrentLightRadius; }
	UFUNCTION(BlueprintCallable)
	float GetMaxRadius() const { return MaxLightRadius; }
	UFUNCTION(BlueprintCallable)
	bool GetIsInvincible() const { return isInvincible; }

	UFUNCTION()
	void OnValidRitm();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UPointLightComponent* Light;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UCapsuleComponent* Capsule;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UNiagaraComponent* NiagaraLeaves;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USON_HealthSystem* HealthSystem;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class UNiagaraSystem* NiagaraTreeComplite;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	class USoundBase* TreeCompliteSound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxLightRadius = 1000;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float StartHP = 20;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float MaxHP = 100;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float LightRadiusExpandSpeed = 500;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float HPIncSpeed = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float GrowRadius = 300;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float LightSourceRadius = 300;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float HPIncRitmDelta = 10;

	virtual void BeginPlay() override;

	UFUNCTION()
	void Death();

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SON_Tower.generated.h"

UCLASS()
class SECRETSOFNORTH_API ASON_Tower : public APawn
{
	GENERATED_BODY()

public:
	ASON_Tower();

	const class UCapsuleComponent* GetCapsule() const { return Capsule; }
	class UStaticMesh* GetStaticMesh() const { return StaticMesh->GetStaticMesh(); }
	bool GetUseMeshCollision() const { return UseMeshCollision;}

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UCapsuleComponent* Capsule;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USON_HealthSystem* HealthSystem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool UseMeshCollision = false;

	virtual void BeginPlay() override;
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_LesserTree.h"

#include "Components/PointLightComponent.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "../../Game/SON_AutobattleGameMode.h"
#include "../Character/SON_PlayerCharacter.h"

ASON_LesserTree::ASON_LesserTree()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	Capsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	Capsule->SetCapsuleSize(42.f, 96.0f);
	Capsule->SetCollisionProfileName(FName("Tower"));
	Capsule->SetGenerateOverlapEvents(true);
	Capsule->bDynamicObstacle = true;
	Capsule->SetCanEverAffectNavigation(true);
	SetRootComponent(Capsule);

	HealthSystem = CreateDefaultSubobject<USON_HealthSystem>(TEXT("HealthSystem"));
	HealthSystem->SetMaxHP(MaxHP);
	HealthSystem->SetCurrentHP(0);
	HealthSystem->OnDied.AddDynamic(this, &ASON_LesserTree::Death);

	Light = CreateDefaultSubobject<UPointLightComponent>(TEXT("Light"));
	Light->SetupAttachment(RootComponent);
	Light->SetAttenuationRadius(0);
	Light->SetSourceRadius(0);
	Light->SetShadowBias(0.5);
	Light->bUseInverseSquaredFalloff = false;
	Light->LightFalloffExponent = 1e-9;
}

void ASON_LesserTree::OnValidRitm()
{
	ASON_AutobattleGameMode* gm = Cast<ASON_AutobattleGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(gm))
	{
		ASON_PlayerCharacter* player = gm->GetPlayerCharacter();
		if (IsValid(player))
		{
			float dist = GetHorizontalDistanceTo(player);
			if (dist <= CurrentLightRadius || dist <= GrowRadius)
				HealthSystem->SetCurrentHP(HealthSystem->GetCurHP() + HPIncRitmDelta);
		}
	}
}

void ASON_LesserTree::BeginPlay()
{
	Super::BeginPlay();
	
	ASON_AutobattleGameMode* gm = Cast<ASON_AutobattleGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(gm))
	{
		gm->AddLesserTree(this);
	}

	HealthSystem->SetMaxHP(MaxHP);
	HealthSystem->SetCurrentHP(0);
	BaseRadius = Light->GetRelativeLocation().Z + Capsule->GetScaledCapsuleHalfHeight();
	BaseIntensity = Light->Intensity;
	Light->Intensity = 0;
	Light->SetAttenuationRadius(0);
	Light->SetSourceRadius(0);
}

void ASON_LesserTree::Death()
{
}

float ASON_LesserTree::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float res = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	HealthSystem->ReduceHP(res);

	return res;
}

void ASON_LesserTree::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ASON_AutobattleGameMode* gm = Cast<ASON_AutobattleGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(gm))
	{
		ASON_PlayerCharacter* player = gm->GetPlayerCharacter();
		if (IsValid(player))
		{
			float dist = GetHorizontalDistanceTo(player);
			if (dist <= CurrentLightRadius || dist <= GrowRadius)
				HealthSystem->SetCurrentHP(HealthSystem->GetCurHP() + HPIncSpeed * DeltaTime);
		}
	}

	if (HealthSystem->GetIsDead())
	{
		Light->Intensity -= LightIntensitySpeed * BaseIntensity * DeltaTime;
		if (Light->Intensity < 0)
			Light->Intensity = 0;
	}
	else
	{
		Light->Intensity += LightIntensitySpeed * BaseIntensity * DeltaTime;
		if (Light->Intensity > BaseIntensity)
			Light->Intensity = BaseIntensity;
	}

	float CurrentTargetLightRadius = MaxLightRadius * HealthSystem->GetCurHP() / HealthSystem->GetMaxHP();

	if (CurrentLightRadius < CurrentTargetLightRadius)
	{
		CurrentLightRadius += DeltaTime * LightRadiusExpandSpeed;
		if (CurrentLightRadius > CurrentTargetLightRadius)
			CurrentLightRadius = CurrentTargetLightRadius;
	}
	if (CurrentLightRadius > CurrentTargetLightRadius)
	{
		CurrentLightRadius -= DeltaTime * LightRadiusExpandSpeed;
		if (CurrentLightRadius < CurrentTargetLightRadius)
			CurrentLightRadius = CurrentTargetLightRadius;
	}

	if (CurrentLightRadius >= MaxLightRadius)
		CurrentLightRadius = MaxLightRadius;

	if (HealthSystem->GetIsDead()) {
		Light->SetAttenuationRadius(0);
		Light->SetSourceRadius(0);
	} else {
		Light->SetAttenuationRadius(sqrt(BaseRadius * BaseRadius + CurrentLightRadius * CurrentLightRadius));
		Light->SetSourceRadius(sqrt(BaseRadius * BaseRadius + CurrentLightRadius * CurrentLightRadius) / 2);
	}
}

void ASON_LesserTree::ActivateTree()
{
	SetActorTickEnabled(true);
}


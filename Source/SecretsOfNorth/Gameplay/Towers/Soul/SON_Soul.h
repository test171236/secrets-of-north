// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../../Common/SON_HealthSystem.h"
#include "SON_Soul.generated.h"

UCLASS()
class SECRETSOFNORTH_API ASON_Soul : public ACharacter
{
	GENERATED_BODY()

	bool isInvincible = true;

	UPROPERTY(EditAnywhere)
	float MaxHP = 10;

public:
	ASON_Soul();

protected:
	virtual void BeginPlay() override;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USON_HealthSystem* HealthSystem;

	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

public:	

	USON_HealthSystem::FOnDied& GetOnDiedDelegate() { return HealthSystem->OnDied; }

};

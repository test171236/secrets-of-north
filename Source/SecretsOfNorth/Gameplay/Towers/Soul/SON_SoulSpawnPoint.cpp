// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_SoulSpawnPoint.h"
#include "Kismet/GameplayStatics.h"
#include "../../../Game/SON_AutobattleGameMode.h"

ASON_SoulSpawnPoint::ASON_SoulSpawnPoint()
{
	PrimaryActorTick.bCanEverTick = false;

}

void ASON_SoulSpawnPoint::BeginPlay()
{
	Super::BeginPlay();
	
	ASON_AutobattleGameMode* game_mode = Cast<ASON_AutobattleGameMode>(
		UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(game_mode))
		game_mode->AddSoulSpawnPoint(this);
}


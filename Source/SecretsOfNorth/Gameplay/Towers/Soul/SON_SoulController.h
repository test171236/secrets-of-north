// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "SON_SoulController.generated.h"

class ASON_LesserTree;
class ASON_Soul;

UCLASS()
class SECRETSOFNORTH_API ASON_SoulController : public AAIController
{
	GENERATED_BODY()
	
	UPROPERTY()
	AActor* Target;
	UPROPERTY()
	ASON_Soul* Soul;

	float AcceptanceRadius = 200;
	bool isMoving = false;

protected:
	virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;
public:
	ASON_SoulController();
	virtual void OnPossess(APawn* Pawn) override;
	virtual void Tick(float DeltaTime) override;
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_SoulController.h"
#include "Kismet/GameplayStatics.h"
#include "Navigation/PathFollowingComponent.h"
#include "../SON_MainTree.h"
#include "../SON_LesserTree.h"
#include "../../../Game/SON_AutobattleGameMode.h"
#include "SON_Soul.h"

ASON_SoulController::ASON_SoulController() 
{
    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.bStartWithTickEnabled = true;
    PrimaryActorTick.TickInterval = 0.1f;
}

void ASON_SoulController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	Soul = Cast<ASON_Soul>(InPawn);
}

void ASON_SoulController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	if (Result.IsSuccess())
	{
		ASON_AutobattleGameMode* gm = Cast<ASON_AutobattleGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		if (IsValid(gm) && Target == gm->GetMainTree())
			gm->SoulArrive(Soul);
		else {
			ASON_LesserTree* tree = Cast<ASON_LesserTree>(Target);
			if (IsValid(tree))
			{
				if (IsValid(tree->GetNextTree()))
					Target = tree->GetNextTree();
				else
					Target = gm->GetMainTree();
				MoveToActor(Target, AcceptanceRadius);
				isMoving = true;
			}
		}
	}
	else
		isMoving = false;
}

void ASON_SoulController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	bool isInLight = false;

	ASON_AutobattleGameMode* game_mode = Cast<ASON_AutobattleGameMode>(
		UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(game_mode) && game_mode->GetState() == ESON_AutobattleGameModeState::SoulGuiding)
	{
		ASON_MainTree* main_tree = game_mode->GetMainTree();
		if (IsValid(main_tree) && main_tree->GetCurrentRadius() >= Soul->GetHorizontalDistanceTo(main_tree))
		{
			if (!isMoving)
			{
				if (!IsValid(Target))
					Target = main_tree;
				MoveToActor(Target, AcceptanceRadius);
				isMoving = true;
			}
			isInLight = true;
		}

		const TArray<ASON_LesserTree*> &tree_arr = game_mode->GetAllLesserTree();
		for (auto &tree : tree_arr)
			if (IsValid(tree) && tree->GetCurrentRadius() >= Soul->GetHorizontalDistanceTo(tree))
			{
				if (!isMoving)
				{
					if (!IsValid(Target))
						Target = tree;
					MoveToActor(Target, AcceptanceRadius);
					isMoving = true;
				}
				isInLight = true;
			}
	}

	if (!isInLight && isMoving)
	{
		StopMovement();
		isMoving = false;
	}
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_Soul.h"
#include "Kismet/GameplayStatics.h"
#include "AIController.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "../../../Game/SON_AutobattleGameMode.h"
#include "../SON_MainTree.h"
#include "SON_SoulController.h"

ASON_Soul::ASON_Soul()
{
	PrimaryActorTick.bCanEverTick = false;

	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	GetCapsuleComponent()->SetCollisionProfileName(FName("PlayerCharacter"));

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 200.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
	GetCharacterMovement()->MaxWalkSpeed = 100;
	GetCharacterMovement()->bUseRVOAvoidance = true;

	HealthSystem = CreateDefaultSubobject<USON_HealthSystem>(TEXT("HealthSystem"));
}

void ASON_Soul::BeginPlay()
{
	Super::BeginPlay();
	
	HealthSystem->SetMaxHP(MaxHP);

	ASON_AutobattleGameMode* game_mode = Cast<ASON_AutobattleGameMode>(
		UGameplayStatics::GetGameMode(GetWorld()));
	if (IsValid(game_mode))
		isInvincible = game_mode->GetIsSoulInvincible();
}

float ASON_Soul::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (isInvincible)
		return 0.f;

	float res = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	HealthSystem->ReduceHP(res);

	return res;
}

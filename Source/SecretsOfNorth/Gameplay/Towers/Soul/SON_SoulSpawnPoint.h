// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SON_SoulSpawnPoint.generated.h"

UCLASS()
class SECRETSOFNORTH_API ASON_SoulSpawnPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	ASON_SoulSpawnPoint();

protected:
	virtual void BeginPlay() override;
};

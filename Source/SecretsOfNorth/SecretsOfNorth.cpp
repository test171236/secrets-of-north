// Copyright Epic Games, Inc. All Rights Reserved.

#include "SecretsOfNorth.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SecretsOfNorth, "SecretsOfNorth" );

DEFINE_LOG_CATEGORY(LogSecretsOfNorth)
 
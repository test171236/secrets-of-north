// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SON_AutobattleGameMode.generated.h"

class ASON_Enemy;
class ASON_Rekken;
class ASON_Abas;
class ASON_MainTree;
class ASON_LesserTree;
class ASON_PlayerCharacter;
class ASON_SoulSpawnPoint;
class ASON_EnemySpawnPoint;
class ASON_Soul;

UENUM(BlueprintType)
enum class ESON_AutobattleGameModeState : uint8 {
	TreeDefence     UMETA(DisplayName = "TreeDefence"),
	SoulGuiding     UMETA(DisplayName = "SoulGuiding"),
	Win				UMETA(DisplayName = "Win"),
	Lose			UMETA(DisplayName = "Lose"),
};	

USTRUCT(BlueprintType)
struct FSON_EnemyParameters {
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float SpawnProb = 0.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<ASON_Enemy> EnemyClass;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float HP_Scale = 1.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float HP_ScaleInc = 0.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Damage_Scale = 1.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Damage_ScaleInc = 0.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float MovementSpeed_Scale = 1.f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float MovementSpeed_ScaleInc = 0.f;
};

UCLASS(minimalapi)
class ASON_AutobattleGameMode : public AGameModeBase
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<ASON_Enemy*> AllEnemies;
	UPROPERTY()
	TArray<ASON_SoulSpawnPoint*> AllSoulSpawnPoints;
	UPROPERTY()
	TArray<ASON_EnemySpawnPoint*> AllEnemySpawnPoints;
	UPROPERTY()
	TArray<ASON_LesserTree*> AllLesserTree;

	UPROPERTY()
	ASON_MainTree* MainTree;
	UPROPERTY()
	ASON_PlayerCharacter* PlayerCharacter;
	UPROPERTY()
	TArray <ASON_Soul*> SoulArray;

	UPROPERTY(EditDefaultsOnly)
	int SoulNum = 1;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASON_Soul> SoulClass;
	UPROPERTY(EditDefaultsOnly)
	TArray<FSON_EnemyParameters> EnemyParameters;
	UPROPERTY(EditDefaultsOnly)
	int MaxEnemyNum = 100;
	UPROPERTY(EditDefaultsOnly)
	float EnemyNormalSpawnRate = 1;
	UPROPERTY(EditDefaultsOnly)
	float EnemyWaveSpawnRate = 100;
	UPROPERTY(EditDefaultsOnly)
	int WaveEnemyNum = 50;
	UPROPERTY(EditDefaultsOnly)
	int EnemyNumUntilWave = 20;
	UPROPERTY(EditDefaultsOnly)
	float StatIncTime = 0.f;
	UPROPERTY(EditDefaultsOnly)
	int MaxPlayerAbilityNum = 4;

	int SoulArrived = 0;
	int WaveEnemySpawned = 0;
	int WaveEnemyDefeated = 0;
	bool isWave = false;

	FTimerHandle EnemySpawnTimer;
	FTimerHandle EnemyWaveSpawnTimer;
	FTimerHandle StatIncTimer;
	
	void StatInc();

	UPROPERTY()
	ESON_AutobattleGameModeState State = ESON_AutobattleGameModeState::TreeDefence;

	UPROPERTY(EditDefaultsOnly)
	float CharacterVisionRadius = 2000;

	UPROPERTY(EditAnywhere)
	bool debugTreeConnects = false;
	UPROPERTY(EditAnywhere)
	bool isSoulInvincible = true;

	UPROPERTY(EditAnywhere)
	USoundBase* BackgroundSound;
	//UPROPERTY(EditDefaultsOnly)
	//TSubclassOf<UUserWidget> LevelNameWidgetClass;

	void StartWave();

	UFUNCTION()
	void OnMainTreeDied();
	UFUNCTION()
	void OnPlayerDied();
	UFUNCTION()
	void OnSoulDied();
public:
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGameEnd, ESON_AutobattleGameModeState, State);
	UPROPERTY(BlueprintAssignable)
	FOnGameEnd OnGameEnd;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTreeComplete);
	UPROPERTY(BlueprintAssignable)
	FOnTreeComplete OnTreeComplete;

	ASON_AutobattleGameMode();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	const TArray<ASON_Enemy*>& GetAllEnemies() const { return AllEnemies; }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const TArray<ASON_SoulSpawnPoint*>& GetAllSoulSpawnPoints() const { return AllSoulSpawnPoints; }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const TArray<ASON_LesserTree*>& GetAllLesserTree() const { return AllLesserTree; }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	ASON_MainTree* GetMainTree() const { return MainTree; }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	ASON_PlayerCharacter* GetPlayerCharacter() const { return PlayerCharacter; }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	const TArray<ASON_Soul*>& GetSouls() const { return SoulArray; }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetMaxEnemyNum() const { return MaxEnemyNum; }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetCurEnemyNum() const { return AllEnemies.Num(); }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetIsSoulInvincible() const { return isSoulInvincible; }
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetMaxPlayerAbilityNum() const { return MaxPlayerAbilityNum; }

	void AddEnemy(ASON_Enemy* Enemy);
	void RemoveEnemy(ASON_Enemy* Enemy);

	void AddLesserTree(ASON_LesserTree* LesserTree);

	void SetupMainTree(ASON_MainTree* NewTree);
	void SetupPlayerCharacter(ASON_PlayerCharacter* NewPlayerCharacter);

	void AddSoulSpawnPoint(ASON_SoulSpawnPoint* Point);
	void AddEnemySpawnPoint(ASON_EnemySpawnPoint* Point);

	UFUNCTION()
	void SpawnSoul();
	UFUNCTION()
	void SetupTrees();
	UFUNCTION()
	void ConnectTrees();
	UFUNCTION()
	void SpawnEnemy();

	void Lose();
	void Win();
	void MainTreeComplite();
	void SoulArrive(class ASON_Soul* Soul);

	virtual void BeginPlay() override;

	ESON_AutobattleGameModeState GetState() const { return State; }
};




// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "../Gameplay/Character/SON_PlayerCharacter.h"
#include "SON_SaveGame.generated.h"

USTRUCT(BlueprintType, Blueprintable)
struct FSON_AbilityBonusSave {
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadOnly)
	FName Name;
	UPROPERTY(BlueprintReadOnly)
	FSON_AbilityBonusParameters AbilityBonusParameters;
};


UCLASS()
class SECRETSOFNORTH_API USON_SaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:

	USON_SaveGame();
	
	UPROPERTY(BlueprintReadWrite)
	int ProgressLevel = 0;
	UPROPERTY(BlueprintReadOnly)
	int CharacterLevel;
	UPROPERTY(BlueprintReadWrite)
	FSON_CharacterStats CharacterStats;
	UPROPERTY(BlueprintReadOnly)
	FSON_CharacterStats CharacterBonuses;
	UPROPERTY(BlueprintReadOnly)
	TArray<FSON_AbilityBonusSave> AbilityBonusSave;

	UFUNCTION()
	float GetCharacterDamage() const { return CharacterStats.Damage + CharacterBonuses.Damage; }
};

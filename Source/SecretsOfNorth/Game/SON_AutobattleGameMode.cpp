// Copyright Epic Games, Inc. All Rights Reserved.

#include "SON_AutobattleGameMode.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"
#include "../Gameplay/Character/SON_PlayerCharacter.h"
#include "../Gameplay/Character/SON_PlayerController.h"
#include "../Gameplay/Character/SON_RitmComponent.h"
#include "../Gameplay/Towers/SON_MainTree.h"
#include "../Gameplay/Towers/SON_LesserTree.h"
#include "../Gameplay/Enemies/SON_Abas.h"
#include "../Gameplay/Enemies/SON_Rekken.h"
#include "../Gameplay/Enemies/SON_EnemySpawnPoint.h"
#include "../Gameplay/Towers/Soul/SON_Soul.h"
#include "../Gameplay/Towers/Soul/SON_SoulSpawnPoint.h"
#include "SON_GameInstance.h"
#include "SON_SaveGame.h"

ASON_AutobattleGameMode::ASON_AutobattleGameMode()
{
}

void ASON_AutobattleGameMode::AddEnemy(ASON_Enemy* Enemy) 
{ 
	AllEnemies.AddUnique(Enemy); 
	if (isWave)
	{
		WaveEnemySpawned++;
		if (WaveEnemySpawned >= WaveEnemyNum)
		{
			isWave = false;
			GetWorld()->GetTimerManager().ClearTimer(EnemyWaveSpawnTimer);
		}
	}
}

void ASON_AutobattleGameMode::RemoveEnemy(ASON_Enemy* Enemy) 
{ 
	AllEnemies.Remove(Enemy); 
	WaveEnemyDefeated++;
	if (WaveEnemyDefeated >= EnemyNumUntilWave + WaveEnemySpawned)
	{
		StartWave();
	}
}

void ASON_AutobattleGameMode::AddLesserTree(ASON_LesserTree* LesserTree)
{
	AllLesserTree.AddUnique(LesserTree);
}

void ASON_AutobattleGameMode::SetupMainTree(ASON_MainTree* NewTree) 
{ 
	if (IsValid(MainTree))
	{
		MainTree->GetOnDiedDelegate().RemoveAll(this);
	}
	MainTree = NewTree;
	MainTree->GetOnDiedDelegate().AddDynamic(this, &ASON_AutobattleGameMode::OnMainTreeDied);
}
void ASON_AutobattleGameMode::SetupPlayerCharacter(ASON_PlayerCharacter* NewPlayerCharacter) 
{ 
	if (IsValid(PlayerCharacter))
	{
		PlayerCharacter->GetOnDiedDelegate().RemoveAll(this);
	}
	PlayerCharacter = NewPlayerCharacter;
	PlayerCharacter->GetOnDiedDelegate().AddDynamic(this, &ASON_AutobattleGameMode::OnPlayerDied);
}

void ASON_AutobattleGameMode::AddSoulSpawnPoint(ASON_SoulSpawnPoint* Point)
{
	AllSoulSpawnPoints.AddUnique(Point);
}

void ASON_AutobattleGameMode::AddEnemySpawnPoint(ASON_EnemySpawnPoint* Point)
{
	AllEnemySpawnPoints.AddUnique(Point);
}

void ASON_AutobattleGameMode::StartWave()
{
	WaveEnemySpawned = 0;
	WaveEnemyDefeated = 0;
	isWave = true;
	GetWorld()->GetTimerManager().SetTimer(EnemyWaveSpawnTimer,
		this,
		&ASON_AutobattleGameMode::SpawnEnemy,
		1.f / EnemyWaveSpawnRate,
		true);

}

void ASON_AutobattleGameMode::OnMainTreeDied()
{
	Lose();
}

void ASON_AutobattleGameMode::OnPlayerDied()
{
	Lose();
}

void ASON_AutobattleGameMode::OnSoulDied()
{
	Lose();
}

void ASON_AutobattleGameMode::SpawnSoul()
{
	TArray<ASON_SoulSpawnPoint*> PossibleSoulSpawnPoints = AllSoulSpawnPoints;

	if (AllSoulSpawnPoints.Num() == 0 || SoulNum <= 0)
		return;

	for (int i = 0; i < SoulNum; i++)
	{
		if (PossibleSoulSpawnPoints.Num() == 0)
			return;

		int ind = FMath::Rand() % PossibleSoulSpawnPoints.Num();
		FHitResult hr;

		UKismetSystemLibrary::LineTraceSingle(
			GetWorld(),
			PossibleSoulSpawnPoints[ind]->GetActorLocation(),
			PossibleSoulSpawnPoints[ind]->GetActorLocation() + FVector(0, 0, -1) * 1e6,
			UEngineTypes::ConvertToTraceType(ECC_GameTraceChannel1),
			false,
			TArray<AActor*>(),
			EDrawDebugTrace::None,
			hr,
			false);

		if (hr.bBlockingHit)
		{
			FVector spawn_loc = hr.ImpactPoint +
				FVector(0, 0, 1) * SoulClass.GetDefaultObject()->GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
			FActorSpawnParameters asp;
			asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			ASON_Soul* Soul = GetWorld()->SpawnActor<ASON_Soul>(SoulClass, FTransform(spawn_loc), asp);
			Soul->GetOnDiedDelegate().AddDynamic(this, &ASON_AutobattleGameMode::OnSoulDied);

			SoulArray.Add(Soul);
		}

		PossibleSoulSpawnPoints.RemoveAt(ind);
	}
}

void ASON_AutobattleGameMode::SetupTrees()
{
	if (IsValid(PlayerCharacter))
	{
		if (IsValid(MainTree))
			PlayerCharacter->RitmComponent->OnRitmCheckValidEvent.AddDynamic(MainTree, &ASON_MainTree::OnValidRitm);
		for (auto tree : AllLesserTree)
			PlayerCharacter->RitmComponent->OnRitmCheckValidEvent.AddDynamic(tree, &ASON_LesserTree::OnValidRitm);
	}
}

void ASON_AutobattleGameMode::ConnectTrees()
{
	if (AllLesserTree.Num() == 0)
		return;

	TArray<ASON_LesserTree*> tree_arr;

	for (auto& tree : AllLesserTree)
		if (MainTree->GetHorizontalDistanceTo(tree) < MainTree->GetMaxRadius() + tree->GetMaxRadius())
		{
			tree_arr.Add(tree);
			tree->SetNextTree(nullptr);
			if (debugTreeConnects)
				DrawDebugLine(GetWorld(),
					MainTree->GetActorLocation(),
					tree->GetActorLocation(),
					FColor::Green,
					true, 0, 0, 1);
		}

	while (tree_arr.Num() > 0)
	{
		ASON_LesserTree* cur_tree = tree_arr[0];
		for (auto& tree : AllLesserTree)
			if (tree != cur_tree && !tree->GetIsNextTreeSet() && cur_tree->GetHorizontalDistanceTo(tree) < cur_tree->GetMaxRadius() + tree->GetMaxRadius())
			{
				tree_arr.Add(tree);
				tree->SetNextTree(cur_tree);
				if (debugTreeConnects)
					DrawDebugLine(GetWorld(),
						cur_tree->GetActorLocation(),
						tree->GetActorLocation(),
						FColor::Green,
						true, 0, 0, 1);
			}
		tree_arr.RemoveAt(0);
	}

	for (int i = 0; i < AllLesserTree.Num(); i++)
		if (!AllLesserTree[i]->GetIsNextTreeSet())
		{
			AllLesserTree[i]->Destroy();
			AllLesserTree.RemoveAt(i);
			i--;
		}
}

void ASON_AutobattleGameMode::SpawnEnemy()
{
	if (AllEnemySpawnPoints.Num() == 0 || AllEnemies.Num() >= MaxEnemyNum)
		return;

	float total_prob = 0;
	for (auto& enemy : EnemyParameters)
		total_prob += enemy.SpawnProb;
	if (total_prob <= 0)
		return;

	const int k = 10;

	FSON_EnemyParameters enemy_to_spawn = EnemyParameters[0];
	float rand = FMath::FRand();
	float prob = 0;
	for (auto& enemy : EnemyParameters)
	{
		prob += enemy.SpawnProb;
		if (rand <= prob / total_prob)
		{
			enemy_to_spawn = enemy;
			break;
		}
	}

	for (int i = 0; i < k; i++)
	{
		int ind = FMath::Rand() % AllEnemySpawnPoints.Num();
		if (AllEnemySpawnPoints[ind]->GetDistanceTo(PlayerCharacter) > CharacterVisionRadius)
		{
			if (AllEnemySpawnPoints[ind]->SpawnEnemy(enemy_to_spawn))
				break;
		}
	}
}

void ASON_AutobattleGameMode::Lose()
{
	if (State != ESON_AutobattleGameModeState::Lose && State != ESON_AutobattleGameModeState::Win)
	{
		State = ESON_AutobattleGameModeState::Lose;
		PlayerCharacter->EmptyBonuses();
		USON_GameInstance* gi = Cast<USON_GameInstance>(GetGameInstance());
		if (IsValid(gi) && IsValid(gi->GameData))
			if (gi->GameData->ProgressLevel != 0)
				gi->GameData->ProgressLevel = 1;
		OnGameEnd.Broadcast(State);
	}
}

void ASON_AutobattleGameMode::Win()
{
	if (State != ESON_AutobattleGameModeState::Lose && State != ESON_AutobattleGameModeState::Win)
	{
		State = ESON_AutobattleGameModeState::Win;
		OnGameEnd.Broadcast(State);
	}
}

void ASON_AutobattleGameMode::MainTreeComplite()
{
	if (State == ESON_AutobattleGameModeState::TreeDefence)
	{
		State = ESON_AutobattleGameModeState::SoulGuiding;
		for (auto& tree : AllLesserTree)
			tree->ActivateTree();
		OnTreeComplete.Broadcast();
	}
}

void ASON_AutobattleGameMode::SoulArrive(ASON_Soul* Soul)
{
	SoulArrived++;
	if (SoulArrived >= SoulArray.Num())
		Win();
}

void ASON_AutobattleGameMode::StatInc() {
	for (auto& ep : EnemyParameters)
	{
		ep.HP_Scale += ep.HP_ScaleInc;
		ep.Damage_Scale += ep.Damage_ScaleInc;
		ep.MovementSpeed_Scale += ep.MovementSpeed_ScaleInc;
	}
}

void ASON_AutobattleGameMode::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimerForNextTick(this, &ASON_AutobattleGameMode::SpawnSoul);
	GetWorld()->GetTimerManager().SetTimerForNextTick(this, &ASON_AutobattleGameMode::SetupTrees);
	GetWorld()->GetTimerManager().SetTimerForNextTick(this, &ASON_AutobattleGameMode::ConnectTrees);
	GetWorld()->GetTimerManager().SetTimer(EnemySpawnTimer,
		this,
		&ASON_AutobattleGameMode::SpawnEnemy,
		1.f / EnemyNormalSpawnRate,
		true);
	GetWorld()->GetTimerManager().SetTimer(StatIncTimer,
		this,
		&ASON_AutobattleGameMode::StatInc,
		StatIncTime,
		true);

	UGameplayStatics::PlaySound2D(GetWorld(), BackgroundSound);

	//UUserWidget* widget = CreateWidget<UUserWidget>(GetWorld(), LevelNameWidgetClass);
	//widget->AddToViewport();
}

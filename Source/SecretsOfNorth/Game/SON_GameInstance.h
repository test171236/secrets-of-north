// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "SON_GameInstance.generated.h"

class USON_SaveGame;

UCLASS()
class SECRETSOFNORTH_API USON_GameInstance : public UGameInstance
{
	GENERATED_BODY()

	virtual void OnStart() override;
	virtual void Shutdown() override;
																	
public:
	
	UPROPERTY(BlueprintReadWrite)
	USON_SaveGame* GameData;

	UFUNCTION(BlueprintCallable)
	FSON_AbilityBonusParameters GetAbilityBonusByName(FName Name);

	UFUNCTION(BlueprintCallable)
	void SetAbilityBonusByName(FName Name, const FSON_AbilityBonusParameters& AbilityBonusParameters);
	UFUNCTION(BlueprintCallable)
	void ResetSaveData();
};

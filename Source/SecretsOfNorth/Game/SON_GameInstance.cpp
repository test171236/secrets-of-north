// Fill out your copyright notice in the Description page of Project Settings.


#include "SON_GameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "SON_SaveGame.h"

void USON_GameInstance::OnStart()
{
    Super::OnStart();

    GameData = Cast<USON_SaveGame>(UGameplayStatics::LoadGameFromSlot("Default", 0));

    if (GameData == nullptr)
    {
        GameData = Cast<USON_SaveGame>(UGameplayStatics::CreateSaveGameObject(USON_SaveGame::StaticClass()));
    }
}

void USON_GameInstance::Shutdown()
{
    UGameplayStatics::SaveGameToSlot(GameData, "Default", 0);
    
    Super::Shutdown();
}

FSON_AbilityBonusParameters USON_GameInstance::GetAbilityBonusByName(FName Name)
{
    if (!IsValid(GameData))
        return FSON_AbilityBonusParameters();

    for (auto& i : GameData->AbilityBonusSave)
        if (i.Name == Name)
            return i.AbilityBonusParameters;

    return FSON_AbilityBonusParameters();
}

void USON_GameInstance::SetAbilityBonusByName(FName Name, const FSON_AbilityBonusParameters& AbilityBonusParameters)
{
    if (!IsValid(GameData))
        return;

    int i;
    for (i = 0; i < GameData->AbilityBonusSave.Num(); i++)
        if (GameData->AbilityBonusSave[i].Name == Name)
            GameData->AbilityBonusSave[i].AbilityBonusParameters = AbilityBonusParameters;

    if (i == GameData->AbilityBonusSave.Num())
    {
        FSON_AbilityBonusSave abs;
        abs.Name = Name;
        abs.AbilityBonusParameters = AbilityBonusParameters;
        GameData->AbilityBonusSave.Add(abs);
    }
}

void USON_GameInstance::ResetSaveData()
{
    //if (IsValid(GameData))
        //GameData->
    GameData = Cast<USON_SaveGame>(UGameplayStatics::CreateSaveGameObject(USON_SaveGame::StaticClass()));
}
